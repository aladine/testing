//==============================================================================
// Casper generated Fri Oct 04 2013 14:45:10 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1920, height: 995};
casper.thenOpen('http://staging.littlelives.com/schools/profile/26832/brighton-montessori-mountbatten');
casper.waitForSelector("h1",
    function success() {
        this.test.assertExists("h1");
        this.click("h1");
    },
    function fail() {
        this.test.assertExists("h1");
});
casper.waitForSelector("h1",
    function success() {
        this.test.assertExists("h1");
        this.click("h1");
    },
    function fail() {
        this.test.assertExists("h1");
});
casper.waitForSelector("h1",
    function success() {
        this.test.assertExists("h1");
        this.click("h1");
    },
    function fail() {
        this.test.assertExists("h1");
});
casper.waitForSelector("h1",
    function success() {
        this.test.assertExists("h1");
        this.click("h1");
    },
    function fail() {
        this.test.assertExists("h1");
});
casper.waitForSelector(x("//*[contains(text(), 'Brighton Montessori')]"),
    function success() {
        this.test.assertExists(x("//*[contains(text(), 'Brighton Montessori')]"));
      },
    function fail() {
        this.test.assertExists(x("//*[contains(text(), 'Brighton Montessori')]"));
});

casper.run(function() {this.test.renderResults(true);});