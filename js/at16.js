//==============================================================================
// Casper generated Fri Oct 04 2013 15:15:22 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
casper.start(casper.options.login_url, function() {
    var data;
    var role = 'owner';//casper.options.role;
    data = {
        'data[User][username]': casper.accounts[role].username,
        'data[User][password]': casper.accounts[role].password
    };
    // console.log(this);
    this.test.assertExists('form[action="/users/login"]', 'login form is found');
    this.fill('form[action="/users/login"]', data, true);
    return this.click('input[type="submit"]');
});
casper.options.viewportSize = {width: 1600, height: 799};
casper.thenOpen('https://staging.littlelives.com/attendances/index/20130705');
var element = 'a.link_day_off';
casper.waitForSelector(element,
    function success() {
        this.test.assertExists(element);
        this.click(element);
    },
    function fail() {
        this.test.assertExists(element);
});
casper.waitForSelector("input[name='name']",
    function success() {
        this.sendKeys("input[name='name']", "hfmd closuremandatory ");
    },
    function fail() {
        this.test.assertExists("input[name='name']");
});
casper.wait(1000);
casper.then(function() {
    this.captureSelector("screenshot1.png", "html");
});

casper.run(function() {this.test.renderResults(true);});