//==============================================================================
// Casper generated Fri Oct 04 2013 15:06:27 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1920, height: 995};
casper.thenOpen('http://staging.littlelives.com/schools/profile/25996/odyssey-fourth-avenue');
casper.waitForSelector(x("//a[normalize-space(text())='Manage your school photos']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Manage your school photos']"));
        this.click(x("//a[normalize-space(text())='Manage your school photos']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Manage your school photos']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='Close']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Close']"));
        this.click(x("//a[normalize-space(text())='Close']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Close']"));
});

casper.run(function() {this.test.renderResults(true);});