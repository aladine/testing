//==============================================================================
// Casper generated Fri Oct 04 2013 08:29:16 GMT+0800 (SGT)
//==============================================================================

var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1195, height: 986};
casper.thenOpen('https://staging.littlelives.com/admin/organisations?h=22571f611a14d2fa3f0acd0f62dc0d39');
casper.waitForSelector("section:nth-child(11) .org_name",
    function success() {
        this.test.assertExists("section:nth-child(11) .org_name");
        this.click("section:nth-child(11) .org_name");
    },
    function fail() {
        this.test.assertExists("section:nth-child(11) .org_name");
});
casper.waitForSelector("#withdrawalReason",
    function success() {
        this.test.assertExists("#withdrawalReason");
        this.click("#withdrawalReason");
    },
    function fail() {
        this.test.assertExists("#withdrawalReason");
});
casper.waitForSelector("textarea[name='other']",
    function success() {
        this.sendKeys("textarea[name='other']", "Going overseas");
    },
    function fail() {
        this.test.assertExists("textarea[name='other']");
});
casper.waitForSelector("form input[name='end_dt']",
    function success() {
        this.test.assertExists("form input[name='end_dt']");
        this.click("form input[name='end_dt']");
    },
    function fail() {
        this.test.assertExists("form input[name='end_dt']");
});
casper.waitForSelector("#btn_dialog_withdraw .ui-button-text",
    function success() {
        this.test.assertExists("#btn_dialog_withdraw .ui-button-text");
        this.click("#btn_dialog_withdraw .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_dialog_withdraw .ui-button-text");
});

casper.run(function() {this.test.renderResults(true);});