//==============================================================================
// Casper generated Fri Oct 04 2013 14:17:32 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1920, height: 1019};
casper.thenOpen('https://staging.littlelives.com/portfolios?h=2253652703d3c4f6e6d977635c25277e');
casper.waitForSelector("#photo_uploader .select_class_wrapper.inner",
    function success() {
        this.test.assertExists("#photo_uploader .select_class_wrapper.inner");
        this.click("#photo_uploader .select_class_wrapper.inner");
    },
    function fail() {
        this.test.assertExists("#photo_uploader .select_class_wrapper.inner");
});
casper.waitForSelector("#photo_uploader #select_class_photo_dialog",
    function success() {
        this.test.assertExists("#photo_uploader #select_class_photo_dialog");
        this.click("#photo_uploader #select_class_photo_dialog");
    },
    function fail() {
        this.test.assertExists("#photo_uploader #select_class_photo_dialog");
});
casper.waitForSelector("#btn_photos_upload .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_upload .ui-button-text");
        this.click("#btn_photos_upload .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_upload .ui-button-text");
});
casper.waitForSelector("#btn_photos_next .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
        this.click("#btn_photos_next .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
});
casper.waitForSelector("#add-photo-dialog #select_album",
    function success() {
        this.test.assertExists("#add-photo-dialog #select_album");
        this.click("#add-photo-dialog #select_album");
    },
    function fail() {
        this.test.assertExists("#add-photo-dialog #select_album");
});
casper.waitForSelector("#add-photo-dialog #select_album",
    function success() {
        this.test.assertExists("#add-photo-dialog #select_album");
        this.click("#add-photo-dialog #select_album");
    },
    function fail() {
        this.test.assertExists("#add-photo-dialog #select_album");
});
casper.waitForSelector("#add-photo-dialog #select_album",
    function success() {
        this.test.assertExists("#add-photo-dialog #select_album");
        this.click("#add-photo-dialog #select_album");
    },
    function fail() {
        this.test.assertExists("#add-photo-dialog #select_album");
});
casper.waitForSelector("#btn_photos_next .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
        this.click("#btn_photos_next .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
});
casper.waitForSelector("input[name='data[MediaFile][0][caption]']",
    function success() {
        this.sendKeys("input[name='data[MediaFile][0][caption]']", " ");
    },
    function fail() {
        this.test.assertExists("input[name='data[MediaFile][0][caption]']");
});
casper.wait(1000);
casper.then(function() {
    this.captureSelector("screenshot1.png", "html");
});
casper.waitForSelector("input[name='data[MediaFile][0][caption]']",
    function success() {
        this.sendKeys("input[name='data[MediaFile][0][caption]']", "A");
    },
    function fail() {
        this.test.assertExists("input[name='data[MediaFile][0][caption]']");
});
casper.wait(1000);
casper.then(function() {
    this.captureSelector("screenshot2.png", "html");
});
casper.waitForSelector("form input[name='data[MediaFile][0][caption]']",
    function success() {
        this.test.assertExists("form input[name='data[MediaFile][0][caption]']");
        this.click("form input[name='data[MediaFile][0][caption]']");
    },
    function fail() {
        this.test.assertExists("form input[name='data[MediaFile][0][caption]']");
});
casper.waitForSelector("input[name='data[MediaFile][1][caption]']",
    function success() {
        this.sendKeys("input[name='data[MediaFile][1][caption]']", " ");
    },
    function fail() {
        this.test.assertExists("input[name='data[MediaFile][1][caption]']");
});
casper.waitForSelector("input[name='data[MediaFile][1][caption]']",
    function success() {
        this.sendKeys("input[name='data[MediaFile][1][caption]']", "B");
    },
    function fail() {
        this.test.assertExists("input[name='data[MediaFile][1][caption]']");
});
casper.waitForSelector("#btn_photos_done .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_done .ui-button-text");
        this.click("#btn_photos_done .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_done .ui-button-text");
});

casper.run(function() {this.test.renderResults(true);});