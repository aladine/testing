var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1600, height: 799};
casper.open('https://staging.littlelives.com/admin/organisations');
casper.waitForSelector(x("//a[normalize-space(text())='Home']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Home']"));
        this.click(x("//a[normalize-space(text())='Home']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Home']"));
});
casper.waitForSelector("form input[name='end_dt']",
    function success() {
        this.test.assertExists("form input[name='end_dt']");
        this.click("form input[name='end_dt']");
    },
    function fail() {
        this.test.assertExists("form input[name='end_dt']");
});
casper.waitForSelector("#btn_dialog_withdraw .ui-button-text",
    function success() {
        this.test.assertExists("#btn_dialog_withdraw .ui-button-text");
        this.click("#btn_dialog_withdraw .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_dialog_withdraw .ui-button-text");
});

casper.run(function() {this.test.renderResults(true);});