//==============================================================================
// Casper generated Fri Oct 04 2013 15:10:42 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================
var element;
var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1920, height: 995};
casper.thenOpen('https://staging.littlelives.com/attendances/roster/25800/20131004?h=2cb2e65ed7469a83e0874847a9d3176e');
element = "#btnEnhancedSave";
casper.waitForSelector(element,
    function success() {
        this.test.assertExists(element);
        this.click(element);
    },
    function fail() {
        this.test.assertExists(element);
});

casper.waitForSelector(x("//a[normalize-space(text())='Saving...']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Saving...']"));
        this.click(x("//a[normalize-space(text())='Saving...']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Saving...']"));
});
casper.waitForSelector(x("//*[contains(text(), 'Present ')]"),
    function success() {
        this.test.assertExists(x("//*[contains(text(), 'Present ')]"));
      },
    function fail() {
        this.test.assertExists(x("//*[contains(text(), 'Present ')]"));
});
casper.waitForSelector(x("//*[contains(text(), 'Absent ')]"),
    function success() {
        this.test.assertExists(x("//*[contains(text(), 'Absent ')]"));
      },
    function fail() {
        this.test.assertExists(x("//*[contains(text(), 'Absent ')]"));
});

casper.run(function() {this.test.renderResults(true);});