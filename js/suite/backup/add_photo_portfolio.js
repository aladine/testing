//==============================================================================
// Casper generated Tue Oct 01 2013 13:47:27 GMT+0800 (SGT)
//==============================================================================
// casperjs --config=./config.json  --includes=./default.js   test add_photo_portfolio.js

var x = require('casper').selectXPath;
// var casper = require('casper').create();
// casper.options.viewportSize = {width: 1220, height: 706};
casper.options.waitTimeout = 10000;
casper.snap(0);

casper.thenOpen('https://staging.littlelives.com/portfolios',function(){
    casper.snap(1);
//     casper.waitForSelector(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common",
//     function success() {
//         this.test.assertExists(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
//         this.click(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
//     },
//     function fail() {
//         this.test.assertExists(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
// });
});

casper.waitForSelector("#photo_uploader .select_class_wrapper.inner",
    function success() {
        this.test.assertExists("#photo_uploader .select_class_wrapper.inner");
        this.click("#photo_uploader .select_class_wrapper.inner");
    },
    function fail() {
        this.test.assertExists("#photo_uploader .select_class_wrapper.inner");
});
casper.waitForSelector("#btn_photos_upload .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_upload .ui-button-text");
        this.click("#btn_photos_upload .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_upload .ui-button-text");
});
casper.waitForSelector("#btn_photos_next .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
        this.click("#btn_photos_next .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
});
casper.waitForSelector("form input[name='MediaAlbumTitle']",
    function success() {
        this.test.assertExists("form input[name='MediaAlbumTitle']");
        this.click("form input[name='MediaAlbumTitle']");
    },
    function fail() {
        this.test.assertExists("form input[name='MediaAlbumTitle']");
});
casper.waitForSelector("input[name='MediaAlbumTitle']",
    function success() {
        this.sendKeys("input[name='MediaAlbumTitle']", "Testing Autumn Fedsticval");
    },
    function fail() {
        this.test.assertExists("input[name='MediaAlbumTitle']");
});
casper.waitForSelector("form .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover",
    function success() {
        this.test.assertExists("form .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover");
        this.click("form .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover");
    },
    function fail() {
        this.test.assertExists("form .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover");
});
casper.waitForSelector("#add-photo-dialog #MediaAlbumDescriptionPhotoDialog",
    function success() {
        this.test.assertExists("#add-photo-dialog #MediaAlbumDescriptionPhotoDialog");
        this.click("#add-photo-dialog #MediaAlbumDescriptionPhotoDialog");
    },
    function fail() {
        this.test.assertExists("#add-photo-dialog #MediaAlbumDescriptionPhotoDialog");
});
casper.waitForSelector("textarea[name='MediaAlbumDescription']",
    function success() {
        this.sendKeys("textarea[name='MediaAlbumDescription']", "Cheng Er");
    },
    function fail() {
        this.test.assertExists("textarea[name='MediaAlbumDescription']");
});
casper.waitForSelector("#btn_photos_next .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
        this.click("#btn_photos_next .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
});
casper.waitForSelector("form input[name='MediaAlbumDate']",
    function success() {
        this.test.assertExists("form input[name='MediaAlbumDate']");
        this.click("form input[name='MediaAlbumDate']");
    },
    function fail() {
        this.test.assertExists("form input[name='MediaAlbumDate']");
});
casper.waitForSelector("#btn_photos_next .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
        this.click("#btn_photos_next .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
});
casper.waitForSelector("form input[name='data[MediaFile][0][caption]']",
    function success() {
        this.test.assertExists("form input[name='data[MediaFile][0][caption]']");
        this.click("form input[name='data[MediaFile][0][caption]']");
    },
    function fail() {
        this.test.assertExists("form input[name='data[MediaFile][0][caption]']");
});
casper.waitForSelector("form input[name='data[MediaFile][0][caption]']",
    function success() {
        this.test.assertExists("form input[name='data[MediaFile][0][caption]']");
        this.click("form input[name='data[MediaFile][0][caption]']");
    },
    function fail() {
        this.test.assertExists("form input[name='data[MediaFile][0][caption]']");
});
casper.waitForSelector("form input[name='data[MediaFile][0][caption]']",
    function success() {
        this.test.assertExists("form input[name='data[MediaFile][0][caption]']");
        this.click("form input[name='data[MediaFile][0][caption]']");
    },
    function fail() {
        this.test.assertExists("form input[name='data[MediaFile][0][caption]']");
});
casper.waitForSelector("#btn_photos_done .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_done .ui-button-text");
        this.click("#btn_photos_done .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_done .ui-button-text");
});

casper.run(function() {this.test.renderResults(true);});