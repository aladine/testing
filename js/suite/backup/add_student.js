//==============================================================================
// Casper generated Mon Sep 30 2013 10:59:54 GMT+0800 (SGT)
//==============================================================================
// casperjs --config=./config.json  --includes=./default.js   test add_student.js
var x = require('casper').selectXPath;

casper.waitForSelector(x("//a[normalize-space(text())='Add Students']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Add Students']"));
        this.click(x("//a[normalize-space(text())='Add Students']"));
        casper.snap(1);
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Add Students']"));
});
casper.waitForSelector("form#OrganisationAdminAddStudentsForm input[name='data[User][1][username]']",
    function success() {
        this.test.assertExists("form#OrganisationAdminAddStudentsForm input[name='data[User][1][username]']");
        this.click("form#OrganisationAdminAddStudentsForm input[name='data[User][1][username]']");
    },
    function fail() {
        this.test.assertExists("form#OrganisationAdminAddStudentsForm input[name='data[User][1][username]']");
});
casper.waitForSelector("input[name='data[User][1][username]']",
    function success() {
        this.sendKeys("input[name='data[User][1][username]']", "trzee@gmalil.com");
    },
    function fail() {
        this.test.assertExists("input[name='data[User][1][username]']");
});
casper.waitForSelector("form#OrganisationAdminAddStudentsForm input[name='data[User][1][name]']",
    function success() {
        this.test.assertExists("form#OrganisationAdminAddStudentsForm input[name='data[User][1][name]']");
        this.click("form#OrganisationAdminAddStudentsForm input[name='data[User][1][name]']");
    },
    function fail() {
        this.test.assertExists("form#OrganisationAdminAddStudentsForm input[name='data[User][1][name]']");
});
casper.waitForSelector("input[name='data[User][1][name]']",
    function success() {
        this.sendKeys("input[name='data[User][1][name]']", "Zee");
    },
    function fail() {
        this.test.assertExists("input[name='data[User][1][name]']");
});
casper.waitForSelector("#User1Class",
    function success() {
        this.test.assertExists("#User1Class");
        this.click("#User1Class");
    },
    function fail() {
        this.test.assertExists("#User1Class");
});
// casper.waitForSelector(".ui-datepicker-calendar .ui-datepicker-other-month.ui-datepicker-unselectable.ui-state-disabled:nth-child(3)",
//     function success() {
//         this.test.assertExists(".ui-datepicker-calendar .ui-datepicker-other-month.ui-datepicker-unselectable.ui-state-disabled:nth-child(3)");
//         this.click(".ui-datepicker-calendar .ui-datepicker-other-month.ui-datepicker-unselectable.ui-state-disabled:nth-child(3)");
//     },
//     function fail() {
//         this.test.assertExists(".ui-datepicker-calendar .ui-datepicker-other-month.ui-datepicker-unselectable.ui-state-disabled:nth-child(3)");
// });
casper.waitForSelector("body",
    function success() {
        this.test.assertExists("body");
        this.click("body");
    },
    function fail() {
        this.test.assertExists("body");
});
casper.waitForSelector(x("//a[normalize-space(text())='Submit']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Submit']"));
        this.click(x("//a[normalize-space(text())='Submit']"));
        // casper.snap(2);
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Submit']"));
});

casper.run(function() {this.test.renderResults(true);});