//==============================================================================
// Casper generated Tue Oct 01 2013 14:24:49 GMT+0800 (SGT)
//==============================================================================
// casperjs --config=./config.json  --includes=./default.js   test add_photo.js

var x = require('casper').selectXPath;
casper.thenOpen('https://staging.littlelives.com/portfolios');

casper.waitForSelector("#btn_add_photo",
    function success() {
        this.test.assertExists("#btn_add_photo");
        this.click("#btn_add_photo");
    },
    function fail() {
        this.test.assertExists("#btn_add_photo");
});


casper.waitForSelector("#btn_photos_upload .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_upload .ui-button-text");
        this.click("#btn_photos_upload .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_upload .ui-button-text");
});
casper.waitForSelector("#btn_photos_next .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
        this.click("#btn_photos_next .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
});
casper.waitForSelector("form input[name='MediaAlbumTitle']",
    function success() {
        this.test.assertExists("form input[name='MediaAlbumTitle']");
        this.click("form input[name='MediaAlbumTitle']");
    },
    function fail() {
        this.test.assertExists("form input[name='MediaAlbumTitle']");
});
casper.waitForSelector("input[name='MediaAlbumTitle']",
    function success() {
        this.sendKeys("input[name='MediaAlbumTitle']", "TAutume  Festival");
    },
    function fail() {
        this.test.assertExists("input[name='MediaAlbumTitle']");
});
casper.waitForSelector("form .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover",
    function success() {
        this.test.assertExists("form .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover");
        this.click("form .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover");
    },
    function fail() {
        this.test.assertExists("form .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover");
});
casper.waitForSelector("#add-photo-dialog #MediaAlbumDescriptionPhotoDialog",
    function success() {
        this.test.assertExists("#add-photo-dialog #MediaAlbumDescriptionPhotoDialog");
        this.click("#add-photo-dialog #MediaAlbumDescriptionPhotoDialog");
    },
    function fail() {
        this.test.assertExists("#add-photo-dialog #MediaAlbumDescriptionPhotoDialog");
});
casper.waitForSelector("textarea[name='MediaAlbumDescription']",
    function success() {
        this.sendKeys("textarea[name='MediaAlbumDescription']", "This si is an  festival");
    },
    function fail() {
        this.test.assertExists("textarea[name='MediaAlbumDescription']");
});
casper.waitForSelector("#btn_photos_next .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
        this.click("#btn_photos_next .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_next .ui-button-text");
});
casper.waitForSelector("#btn_photos_done .ui-button-text",
    function success() {
        this.test.assertExists("#btn_photos_done .ui-button-text");
        this.click("#btn_photos_done .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_photos_done .ui-button-text");
});

casper.run(function() {this.test.renderResults(true);});