//==============================================================================
// Casper generated Tue Oct 01 2013 14:45:50 GMT+0800 (SGT)
//==============================================================================

var x = require('casper').selectXPath;
// var casper = require('casper').create();
casper.options.viewportSize = {width: 1220, height: 706};
casper.thenOpen('https://staging.littlelives.com/admin/organisations?h=788f0fc18f0aa333c10579c423da832e');
casper.waitForSelector(x("//a[normalize-space(text())='Add Staff']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Add Staff']"));
        this.click(x("//a[normalize-space(text())='Add Staff']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Add Staff']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='Save']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Save']"));
        this.click(x("//a[normalize-space(text())='Save']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Save']"));
});

casper.waitForSelector("strong:nth-child(3)",
    function success() {
        this.test.assertExists("strong:nth-child(3)");
        this.click("strong:nth-child(3)");
    },
    function fail() {
        this.test.assertExists("strong:nth-child(3)");
});
casper.waitForSelector("strong:nth-child(3)",
    function success() {
        this.test.assertExists("strong:nth-child(3)");
        this.click("strong:nth-child(3)");
    },
    function fail() {
        this.test.assertExists("strong:nth-child(3)");
});
casper.waitForSelector("strong:nth-child(3)",
    function success() {
        this.test.assertExists("strong:nth-child(3)");
        this.click("strong:nth-child(3)");
    },
    function fail() {
        this.test.assertExists("strong:nth-child(3)");
});

casper.run(function() {this.test.renderResults(true);});