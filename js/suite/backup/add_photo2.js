//==============================================================================
// Casper generated Tue Oct 01 2013 14:37:54 GMT+0800 (SGT)
//==============================================================================
// casperjs --config=./config.json  --includes=./default.js   test add_photo2.js

var x = require('casper').selectXPath;
// var casper = require('casper').create();
casper.options.viewportSize = {width: 1220, height: 706};
casper.thenOpen('https://staging.littlelives.com/portfolios/index/26744?t=1380608673995');
casper.then(function() {
    this.mouse.click(835, 487);
});
casper.then(function() {
    this.mouse.click(842, 356);
});
casper.waitForSelector("form input[name='MediaAlbumTitle']",
    function success() {
        this.test.assertExists("form input[name='MediaAlbumTitle']");
        this.click("form input[name='MediaAlbumTitle']");
    },
    function fail() {
        this.test.assertExists("form input[name='MediaAlbumTitle']");
});
casper.waitForSelector("input[name='MediaAlbumTitle']",
    function success() {
        this.sendKeys("input[name='MediaAlbumTitle']", "testAutumn ");
    },
    function fail() {
        this.test.assertExists("input[name='MediaAlbumTitle']");
});
casper.wait(1000);
casper.then(function() {
    this.captureSelector("screenshot1.png", "html");
});
casper.waitForSelector("input[name='MediaAlbumTitle']",
    function success() {
        this.sendKeys("input[name='MediaAlbumTitle']", "on");
    },
    function fail() {
        this.test.assertExists("input[name='MediaAlbumTitle']");
});
casper.wait(1000);
casper.then(function() {
    this.captureSelector("screenshot2.png", "html");
});
casper.waitForSelector("input[name='MediaAlbumTitle']",
    function success() {
        this.sendKeys("input[name='MediaAlbumTitle']", "o");
    },
    function fail() {
        this.test.assertExists("input[name='MediaAlbumTitle']");
});
casper.wait(1000);
casper.then(function() {
    this.captureSelector("screenshot3.png", "html");
});
casper.wait(1000);
casper.then(function() {
    this.captureSelector("screenshot4.png", "html");
});
casper.wait(1000);
casper.then(function() {
    this.captureSelector("screenshot5.png", "html");
});
casper.waitForSelector("input[name='MediaAlbumTitle']",
    function success() {
        this.sendKeys("input[name='MediaAlbumTitle']", "asonata");
    },
    function fail() {
        this.test.assertExists("input[name='MediaAlbumTitle']");
});
casper.waitForSelector("form .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover",
    function success() {
        this.test.assertExists("form .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover");
        this.click("form .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover");
    },
    function fail() {
        this.test.assertExists("form .ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover");
});
casper.then(function() {
    this.mouse.click(410, 558);
});
casper.waitForSelector("textarea[name='MediaAlbumDescription']",
    function success() {
        this.sendKeys("textarea[name='MediaAlbumDescription']", "Testing");
    },
    function fail() {
        this.test.assertExists("textarea[name='MediaAlbumDescription']");
});
casper.then(function() {
    this.mouse.down(847, 678);
    this.mouse.move(848, 677);
    this.mouse.up(848, 677);
});
casper.then(function() {
    this.mouse.click(854, 664);
});
casper.then(function() {
    this.mouse.click(866, 364);
});

casper.run(function() {this.test.renderResults(true);});