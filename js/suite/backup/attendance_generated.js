//==============================================================================
// Casper generated Fri Sep 27 2013 19:03:30 GMT+0800 (SGT)
//==============================================================================

var x = require('casper').selectXPath;
var casper = require('casper').create();
casper.options.viewportSize = {width: 1276, height: 683};
casper.start('https://staging.littlelives.com/attendances/roster/26286/20130717');
casper.waitForSelector(x("//a[normalize-space(text())='PrevNextJuly 2013SuMoTuWeThFrSa 12345678910111213141516171819202122232425262728293031 17 Jul 2013 Select date']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='PrevNextJuly 2013SuMoTuWeThFrSa 12345678910111213141516171819202122232425262728293031 17 Jul 2013 Select date']"));
        this.click(x("//a[normalize-space(text())='PrevNextJuly 2013SuMoTuWeThFrSa 12345678910111213141516171819202122232425262728293031 17 Jul 2013 Select date']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='PrevNextJuly 2013SuMoTuWeThFrSa 12345678910111213141516171819202122232425262728293031 17 Jul 2013 Select date']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='present']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='present']"));
        this.click(x("//a[normalize-space(text())='present']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='present']"));
});
casper.waitForSelector(x("//a[@id='remark_d7702010-c10f-11e2-9f78-bf2b9980b50b']"),
    function success() {
        this.test.assertExists(x("//a[@id='remark_d7702010-c10f-11e2-9f78-bf2b9980b50b']"));
        this.click(x("//a[@id='remark_d7702010-c10f-11e2-9f78-bf2b9980b50b']"));
    },
    function fail() {
        this.test.assertExists(x("//a[@id='remark_d7702010-c10f-11e2-9f78-bf2b9980b50b']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='Cough']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Cough']"));
        this.click(x("//a[normalize-space(text())='Cough']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Cough']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='Rasheslegs']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Rasheslegs']"));
        this.click(x("//a[normalize-space(text())='Rasheslegs']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Rasheslegs']"));
});
casper.waitForSelector(".btn_affirm.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-hover .ui-button-text",
    function success() {
        this.test.assertExists(".btn_affirm.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-hover .ui-button-text");
        this.click(".btn_affirm.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-hover .ui-button-text");
    },
    function fail() {
        this.test.assertExists(".btn_affirm.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-hover .ui-button-text");
});
casper.waitForSelector(".btn_action.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-hover .ui-button-text",
    function success() {
        this.test.assertExists(".btn_action.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-hover .ui-button-text");
        this.click(".btn_action.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-hover .ui-button-text");
    },
    function fail() {
        this.test.assertExists(".btn_action.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-hover .ui-button-text");
});
casper.waitForSelector(x("//a[normalize-space(text())='Saving...']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Saving...']"));
        this.click(x("//a[normalize-space(text())='Saving...']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Saving...']"));
});

casper.run(function() {this.test.renderResults(true);});