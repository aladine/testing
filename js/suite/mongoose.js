var mongoose = require ("mongoose"); // The reason for this demo.
var uristring = 'mongodb://littledan:qwerty@ds047468.mongolab.com:47468/testcases';
mongoose.connect(uristring, function (err, res) {
  if (err) {
    console.log ('ERROR connecting to: ' + uristring + '. ' + err);
  } else {
    console.log ('Succeeded connected to: ' + uristring);
  }
});


// This is the schema.  Note the types, validation and trim
// statements.  They enforce useful constraints on the data.
var userSchema = new mongoose.Schema(
{
    "nid": { type: Number, min: 0},
     "product": { type: String, trim: true },
    "expected_results": { type: String, trim: true },
    "account": { type: String, trim: true },
    "url": { type: String, trim: true },
    "exist": { type: String, trim: true },
    "fields": { type: String, trim: true },
    "tid": { type: String, trim: true }
}
);


// Compiles the schema into a model, opening (or creating, if
// nonexistent) the 'PowerUsers' collection in the MongoDB database
var PUser = mongoose.model('Testcases', userSchema);