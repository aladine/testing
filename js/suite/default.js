var casper, tm;

casper = require('casper').create();

casper.on('remote.message', function(msg) {
  // return this.echo("Remote  " + msg, "INFO");
});

casper.on('page.error', function(msg, trace) {
  return this.echo("Page Error " + msg, "ERROR");
});

casper.on("http.status.200", function(resource) {
  return this.echo("" + resource.url + " is OK", "INFO");
});

casper.on("http.status.301", function(resource) {
  return this.echo("" + resource.url + " is permanently redirected", "PARAMETER");
});

casper.on("http.status.302", function(resource) {
  return this.echo("" + resource.url + " is temporarily redirected", "PARAMETER");
});

casper.on("http.status.404", function(resource) {
  return this.echo("" + resource.url + " is not found", "COMMENT");
});

casper.on("http.status.500", function(resource) {
  return this.echo("" + resource.url + " is in error", "ERROR");
});

casper.options.pageSettings["loadImages"] = false;
casper.options.pageSettings["loadPlugins"] = false;
casper.options.pageSettings["userName"] = "developers";
casper.options.pageSettings["password"] = "1qazxsw2";
casper.options.login_url = 'https://staging.littlelives.com/users/login';
// casper.options.role = 'twinkle';

casper.accounts = {
  twinkle: {
    username: "twinkle@littlelives.com",
    password: "littlelives"
  },
  tchr: {
    username: "pcf-buvanesh",
    password: "littlelives"
  },
  owner: {
    username: "padma@shkindy.com",
    password: "littlelives"
  },
  prnt: {
    username: "leeminhoo@gmail.com",
    password: "littlelives"
  },
  superadmin: {
    username: "trongdan@littlelives.com",
    password: "littledan"
  },
  stdnt: {
    username: "zee@primary.sch",
    password: "littlelives"
  }
};

tm = new Date();

tm = tm.getFullYear() + "_" + (tm.getMonth() + 1) + "_" + tm.getDate() + "_" + tm.getHours() + "_" + tm.getMinutes();

casper.snap = function(tid) {
  var n;
  n = new Date();
  this.viewport(1024, 768);
  this.capture("screenshot/" + tm + "_" + tid + ".png", {
    top: 0,
    left: 0,
    width: 1024,
    height: 768
  });
  return this.echo("Snapshot done at " + n.getHours() + ':' + n.getMinutes() + ':' + n.getSeconds());
};

casper.convertLocalUrl = function(e) {
  return e.replace("staging.littlelives.com", "192.168.1.55").replace("https", "http");
};

var fn_login = function() {
    var data;
    var role = 'twinkle';//casper.options.role;
    data = {
        'data[User][username]': casper.accounts[role].username,
        'data[User][password]': casper.accounts[role].password
    };
    // console.log(this);
    this.test.assertExists('form[action="/users/login"]', 'login form is found');
    this.fill('form[action="/users/login"]', data, true);
    return this.click('input[type="submit"]');
};
