//==============================================================================
// Casper generated Fri Oct 04 2013 14:29:52 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1920, height: 1019};
casper.thenOpen('https://staging.littlelives.com/portfolios?h=2253652703d3c4f6e6d977635c25277e');
casper.waitForSelector("body",
    function success() {
        this.test.assertExists("body");
        this.click("body");
    },
    function fail() {
        this.test.assertExists("body");
});
casper.waitForSelector("form input[name='video_file']",
    function success() {
        this.test.assertExists("form input[name='video_file']");
        this.click("form input[name='video_file']");
    },
    function fail() {
        this.test.assertExists("form input[name='video_file']");
});
casper.waitForSelector("#btn_video_upload .ui-button-text",
    function success() {
        this.test.assertExists("#btn_video_upload .ui-button-text");
        this.click("#btn_video_upload .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_video_upload .ui-button-text");
});
casper.waitForSelector("form input[name='video_file']",
    function success() {
        this.test.assertExists("form input[name='video_file']");
        this.click("form input[name='video_file']");
    },
    function fail() {
        this.test.assertExists("form input[name='video_file']");
});
casper.waitForSelector("#btn_video_upload .ui-button-text",
    function success() {
        this.test.assertExists("#btn_video_upload .ui-button-text");
        this.click("#btn_video_upload .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_video_upload .ui-button-text");
});
casper.waitForSelector("#btn_video_upload .ui-button-text",
    function success() {
        this.test.assertExists("#btn_video_upload .ui-button-text");
        this.click("#btn_video_upload .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_video_upload .ui-button-text");
});
casper.waitForSelector(x("//*[contains(text(), 'Please choose video type file')]"),
    function success() {
        this.test.assertExists(x("//*[contains(text(), 'Please choose video type file')]"));
      },
    function fail() {
        this.test.assertExists(x("//*[contains(text(), 'Please choose video type file')]"));
});

casper.run(function() {this.test.renderResults(true);});