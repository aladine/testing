//==============================================================================
// Casper generated Fri Oct 04 2013 14:55:19 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1920, height: 1019};
casper.thenOpen('https://staging.littlelives.com/portfolios?h=94ca92546784c6ffff5b3fdf5a021dae');
casper.waitForSelector(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common",
    function success() {
        this.test.assertExists(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
        this.click(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
    },
    function fail() {
        this.test.assertExists(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
});
casper.waitForSelector(".org_chart_wrapper",
    function success() {
        this.test.assertExists(".org_chart_wrapper");
        this.click(".org_chart_wrapper");
    },
    function fail() {
        this.test.assertExists(".org_chart_wrapper");
});
casper.waitForSelector(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common",
    function success() {
        this.test.assertExists(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
        this.click(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
    },
    function fail() {
        this.test.assertExists(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
});
casper.waitForSelector(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common",
    function success() {
        this.test.assertExists(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
        this.click(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
    },
    function fail() {
        this.test.assertExists(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
});
casper.waitForSelector(x("//a[normalize-space(text())='Evaluation']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Evaluation']"));
        this.click(x("//a[normalize-space(text())='Evaluation']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Evaluation']"));
});
casper.waitForSelector(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common",
    function success() {
        this.test.assertExists(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
        this.click(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
    },
    function fail() {
        this.test.assertExists(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
});
casper.waitForSelector(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common",
    function success() {
        this.test.assertExists(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
        this.click(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
    },
    function fail() {
        this.test.assertExists(".jqtree_common.jqtree-selected .jqtree-title.jqtree_common");
});
casper.waitForSelector(x("//a[normalize-space(text())='Evaluation']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Evaluation']"));
        this.click(x("//a[normalize-space(text())='Evaluation']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Evaluation']"));
});
casper.waitForSelector(".checklist_area tr:nth-child(32) .res.selected",
    function success() {
        this.test.assertExists(".checklist_area tr:nth-child(32) .res.selected");
        this.click(".checklist_area tr:nth-child(32) .res.selected");
    },
    function fail() {
        this.test.assertExists(".checklist_area tr:nth-child(32) .res.selected");
});
casper.waitForSelector(x("//a[normalize-space(text())='Save Evaluation']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Save Evaluation']"));
        this.click(x("//a[normalize-space(text())='Save Evaluation']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Save Evaluation']"));
});

casper.run(function() {this.test.renderResults(true);});