//==============================================================================
// Casper generated Fri Oct 04 2013 14:22:41 GMT+0800 (SGT)
//==============================================================================

var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1195, height: 467};
casper.thenOpen('https://staging.littlelives.com/attendances/roster/26744/20131003');
casper.waitForSelector(x("//a[@id='remark_51b7eb72-7b6c-44fe-a006-24c6740c3493']"),
    function success() {
        this.test.assertExists(x("//a[@id='remark_51b7eb72-7b6c-44fe-a006-24c6740c3493']"));
        this.click(x("//a[@id='remark_51b7eb72-7b6c-44fe-a006-24c6740c3493']"));
    },
    function fail() {
        this.test.assertExists(x("//a[@id='remark_51b7eb72-7b6c-44fe-a006-24c6740c3493']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='Cough']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Cough']"));
        this.click(x("//a[normalize-space(text())='Cough']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Cough']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='Red Spotstongue']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Red Spotstongue']"));
        this.click(x("//a[normalize-space(text())='Red Spotstongue']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Red Spotstongue']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='Illness']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Illness']"));
        this.click(x("//a[normalize-space(text())='Illness']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Illness']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='Chicken Pox']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Chicken Pox']"));
        this.click(x("//a[normalize-space(text())='Chicken Pox']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Chicken Pox']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='Misc Status']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Misc Status']"));
        this.click(x("//a[normalize-space(text())='Misc Status']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Misc Status']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='Late']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Late']"));
        this.click(x("//a[normalize-space(text())='Late']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Late']"));
});
casper.waitForSelector("#btn_done_remark .ui-button-text",
    function success() {
        this.test.assertExists("#btn_done_remark .ui-button-text");
        this.click("#btn_done_remark .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_done_remark .ui-button-text");
});
casper.waitForSelector(x("//a[normalize-space(text())='Saving...']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Saving...']"));
        this.click(x("//a[normalize-space(text())='Saving...']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Saving...']"));
});

casper.run(function() {this.test.renderResults(true);});