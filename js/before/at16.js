//==============================================================================
// Casper generated Fri Oct 04 2013 15:15:22 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1600, height: 799};
casper.thenOpen('https://staging.littlelives.com/attendances/index/20130705');
casper.waitForSelector(x("//a[normalize-space(text())='Declare day off for St. Hilda's Kindergarten']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Declare day off for St. Hilda's Kindergarten']"));
        this.click(x("//a[normalize-space(text())='Declare day off for St. Hilda's Kindergarten']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Declare day off for St. Hilda's Kindergarten']"));
});
casper.waitForSelector("input[name='name']",
    function success() {
        this.sendKeys("input[name='name']", "hfmd closuremandatory ");
    },
    function fail() {
        this.test.assertExists("input[name='name']");
});
casper.wait(1000);
casper.then(function() {
    this.captureSelector("screenshot1.png", "html");
});

casper.run(function() {this.test.renderResults(true);});