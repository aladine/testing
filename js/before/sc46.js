//==============================================================================
// Casper generated Fri Oct 04 2013 14:04:59 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1920, height: 995};
casper.thenOpen('http://staging.littlelives.com/schools');
casper.waitForSelector("form input[name='data[loc]']",
    function success() {
        this.test.assertExists("form input[name='data[loc]']");
        this.click("form input[name='data[loc]']");
    },
    function fail() {
        this.test.assertExists("form input[name='data[loc]']");
});
casper.waitForSelector("input[name='data[loc]']",
    function success() {
        this.sendKeys("input[name='data[loc]']", "dakota");
    },
    function fail() {
        this.test.assertExists("input[name='data[loc]']");
});
casper.waitForSelector("form input[name='level']",
    function success() {
        this.test.assertExists("form input[name='level']");
        this.click("form input[name='level']");
    },
    function fail() {
        this.test.assertExists("form input[name='level']");
});
casper.waitForSelector("form input[name='data[query]']",
    function success() {
        this.test.assertExists("form input[name='data[query]']");
        this.click("form input[name='data[query]']");
    },
    function fail() {
        this.test.assertExists("form input[name='data[query]']");
});
casper.waitForSelector("input[name='data[query]']",
    function success() {
        this.sendKeys("input[name='data[query]']", "talents\r");
    },
    function fail() {
        this.test.assertExists("input[name='data[query]']");
});

casper.run(function() {this.test.renderResults(true);});