//==============================================================================
// Casper generated Fri Oct 04 2013 14:22:55 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1920, height: 995};
casper.thenOpen('https://staging.littlelives.com/questions?h=94ca92546784c6ffff5b3fdf5a021dae');
casper.waitForSelector("#ContentContent",
    function success() {
        this.test.assertExists("#ContentContent");
        this.click("#ContentContent");
    },
    function fail() {
        this.test.assertExists("#ContentContent");
});
casper.waitForSelector(x("//*[contains(text(), 'Your answer cannot be empty')]"),
    function success() {
        this.test.assertExists(x("//*[contains(text(), 'Your answer cannot be empty')]"));
      },
    function fail() {
        this.test.assertExists(x("//*[contains(text(), 'Your answer cannot be empty')]"));
});

casper.run(function() {this.test.renderResults(true);});