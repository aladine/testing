//==============================================================================
// Casper generated Fri Oct 04 2013 14:17:15 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
casper.start(casper.options.login_url, fn_login);
casper.options.viewportSize = {width: 1920, height: 995};
casper.thenOpen('http://staging.littlelives.com/schools');
casper.waitForSelector(".filter-container",
    function success() {
        this.test.assertExists(".filter-container");
        this.click(".filter-container");
    },
    function fail() {
        this.test.assertExists(".filter-container");
});
casper.waitForSelector(x("//a[normalize-space(text())='Name']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Name']"));
        this.click(x("//a[normalize-space(text())='Name']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Name']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='Latest']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Latest']"));
        this.click(x("//a[normalize-space(text())='Latest']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Latest']"));
});

casper.run(function() {this.test.renderResults(true);});