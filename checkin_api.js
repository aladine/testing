var  async = require('async'),
    request = require('request'),
    _accessToken,_refressToken;
//url http://littlelives.com/oauth/token?grant_type=password&username=chet@littlelives.com&password=littlelives&client_id=NTA3NDE3Yzg5Njc3NWIz&client_secret=0ad52feb99e6df3bdef877ef40348e0c
/*
1. Get an Authorization code
http://www.littlelives.com/oauth/authorize?response_type=code&client_id=xxxx&redirect_url=http://www.your-redirect-url-here
2. Swap Code for access token
http://www.littlelives.com/oauth/token?grant_type=authorization_code&code=from_above&client_id=xxxx&client_secret=xxxx
3. Use Access Token 
http://www.littlelives.com/api/users/sample/?access_token=e794842fbe659c4448d69aaf1084cd0678927af
*/
var USERNAME = 'chet@littlelives.com';
var PASSWORD = 'littlelives';
var CLIENT_ID = 'NTA3NDE3Yzg5Njc3NWIz';
var CLIENT_SECRET = '0ad52feb99e6df3bdef877ef40348e0c';
var DOMAIN = 'http://littlelives.com' ;
var OATH_URL = DOMAIN + '/oauth/token';

function logStatus(info) {
    console.log('\x1b[36m%s\x1b[0m', info);
}

function logCommand(meth, path) {
    console.log(' > \x1b[33m%s\x1b[0m: %s', meth, path);
}

async.waterfall([
  //-----------------------------
  // Obtain a new access token
  //-----------------------------
  function(callback) {
    logCommand('GET',OATH_URL);
    request.get({
      'url': OATH_URL,
      'qs': {
        grant_type: 'password',
        username: USERNAME,
        password: PASSWORD,
        client_id: CLIENT_ID
        // client_secret: CLIENT_SECRET
      }
    }, function(e,r,body){
      callback(null,body);
    });
  },

  //----------------------------
  // Parse the response
  //----------------------------
  function(response, callback) {
    var res =  JSON.parse(response);
    if (response.error) {
      return callback(list.error);
    }
    _accessToken = res.access_token;
    _refreshToken = res.refresh_token;
    logStatus('_accessToken:'+_accessToken);
    logStatus('_refreshToken:'+_refreshToken);
    logCommand('GET',DOMAIN+'/api/attendances/checkin_2_0');
    var n = new Date();
     request.get({
      'url': DOMAIN+'/api/attendances/checkin_2_0',
      'qs': {
        user_id: '511c7a57-6eac-4a18-a7e4-4025740c3493' ,
        organisation_id: 26261  ,
        checkin_day: '20131117',
        checkin_hour: n.getHours() ,
        checkin_min:   n.getMinutes() ,
        temperature : '37.4'  ,
        source: 'http://littlelives.production.s3.amazonaws.com/users/checkin/5845caa8-d170-11e2-9f78-bf2b9980b50b/checkin_1379033541.711143.jpg',
        access_token: _accessToken
      }
    }, function(e,r,body){
      // console.log(body);
      callback(null,body);
    });
    
  }
], function(err, results) {
  if (!err) {
    logStatus(results.status);
    console.log(results);
  }
});