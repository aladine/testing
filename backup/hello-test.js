// hello-test.js
// var require = patchRequire(require);
// var _ = require('../underscore');
// casper.log(,'debug')
var casper = require('casper').create({
	verbose: true,
  logLevel: 'debug',
  pageSettings: {
       loadImages:  false,         // The WebPage instance used by Casper will
       loadPlugins: false,         // use these settings
       // userName: 'developers',
       // password: '1qazxsw2',
       userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
  }
});


// print out all the messages in the headless browser context
casper.on('remote.message', function(msg) {
    //this.echo('Remote message caught: ' + msg);
});

// print out all the messages in the headless browser context
casper.on("page.error", function(msg, trace) {
    this.echo("Page Error: " + msg, "ERROR");
});

// casper.test.begin("Hello, Test!", 1, function(test) {
//   test.assert(true);
//   test.done();
// });

var AUTH_ERROR = 'You are not authorized to access that location.';

var cases =
[
  {
    "id":1,
    "title":"Admin",
    "url":"https://staging.littlelives.com/admin/organisations",
    "element":"//*[@id=\"btn_dialog_withdraw\"]"
  },
  {
    "id":2,
    "title":"Admin",
    "url":"https://staging.littlelives.com/admin/organisations",
    "element":"//*[@id=\"btn_dialog_withdraw\"]"
  },
  {
    "id":3,
    "title":"Add Class",
    "url":"https://staging.littlelives.com/admin/organisations/add_class",
    "element":"//*[@id=\"btnSubmit\"]"
  },
  {
    "id":4,
    "title":"Add User",
    "url":"https://staging.littlelives.com/admin/organisations/add_user",
    "element":"//*[@id=\"btnProceed\"] \n//*[@id=\"btnSave\"]\n\n"
  },
  {
    "id":5,
    "title":"",
    "url":"https://staging.littlelives.com/admin/organisations/add_user",
    "element":"//*[@id=\"btnProceed\"]"
  },
  {
    "id":6,
    "title":"Add People",
    "url":"https://staging.littlelives.com/admin/organisations/add_user",
    "element":"//a[@id='btnProceed' and @class='btn_affirm']"
  },
  {
    "id":7,
    "title":"Add Multiple Students",
    "url":"https://staging.littlelives.com/admin/organisations/add_students",
    "element":"//a[@id='btnProceed' and @class='btn_affirm']"
  },
  // {
  //   "id":8,
  //   "title":"Admin",
  //   "url":"",
  //   "element":"//button[@id='btn_dialog_transfer']"
  // },
  {
    "id":9,
    "title":"Admin",
    "url":"https://staging.littlelives.com/admin/organisations#wh",
    "element":"a[@class='btn_affirm btn_save_wh']"
  },
  {
    "id":10,
    "title":"Add Multiple Students",
    "url":"https://staging.littlelives.com/admin/organisations/add_students",
    "element":"//a[@id='btnProceed' and @class='btn_affirm']"
  },
  {
    "id":11,
    "title":"Admin",
    "url":"https://staging.littlelives.com/admin/organisations",
    "element":"/html/body/div/div[3]/div[2]/div/section/div[2]/table/tfoot/tr/td/a\n//*[@id=\"btn_send_sms\"]"
  },
  {
    "id":12,
    "title":"Add students",
    "url":"https://staging.littlelives.com/admin/organisations/add_students",
    "element":"//*[@id=\"btnProceed\"]"
  },
  {
    "id":13,
    "title":"Cheerfulness",
    "url":"https://staging.littlelives.com/attendances/roster/26600/20130702",
    "element":"//*[@id=\"btnSave\"]"
  },
  {
    "id":14,
    "title":"St.Hilda's Kindergarten",
    "url":"https://staging.littlelives.com/attendances/roster/26537/20130702",
    "element":"//*[@id=\"btnEnhancedSave\"]"
  },
  {
    "id":14,
    "title":"Attendance",
    "url":"https://staging.littlelives.com/attendances/roster/26537/20130702",
    "element":"/html/body/div[2]/div[11]/div/button[2]"
  },
  {
    "id":17,
    "title":"Attendance",
    "url":"https://staging.littlelives.com/attendances/index/20130705",
    "element":"/html/body/div[2]/div[3]/div/button[2]"
  },
  {
    "id":18,
    "title":"Attendance",
    "url":"https://staging.littlelives.com/attendances/index/20130705",
    "element":"/html/body/div[2]/div/div[2]/div/div/p/a"
  },
  // {
  //   "id":1,
  //   "title":"LittleLives",
  //   "url":"https://staging.littlelives.com/users/login",
  //   "element":"/html/body/div/div/form/div[2]/fieldset/div[3]/input"
  // },
  // {
  //   "id":6,
  //   "title":"LittleLives",
  //   "url":"https://staging.littlelives.com/users/forgetpassword",
  //   "element":"/html/body/div/div/form/div[2]/fieldset/div[2]/input"
  // },
  {
    "id":12,
    "title":"Notifications",
    "url":"https://staging.littlelives.com/notifications",
    "element":""
  },
  {
    "id":14,
    "title":"Notifications",
    "url":"https://staging.littlelives.com/notifications",
    "element":""
  },
  {
    "id":16,
    "title":"Notifications",
    "url":"https://staging.littlelives.com/notifications",
    "element":""
  },
  {
    "id":17,
    "title":"Admin",
    "url":"https://staging.littlelives.com/admin/organisations",
    "element":"//*[@id=\"btn_dialog_transfer\"]"
  },
  {
    "id":18,
    "title":"Admin",
    "url":"https://staging.littlelives.com/admin/organisations",
    "element":"//*[@id=\"btn_dialog_transfer\"]"
  },
  {
    "id":22,
    "title":"Add Photos",
    "url":"https://staging.littlelives.com/portfolios",
    "element":"//*[@id=\"btn_photos_upload\"]"
  },
  {
    "id":23,
    "title":"Add Videos",
    "url":"https://staging.littlelives.com/portfolios",
    "element":"//*[@id=\"btn_video_upload\"]"
  },
  {
    "id":24,
    "title":"Add Videos",
    "url":"https://staging.littlelives.com/portfolios",
    "element":"//*[@id=\"btn_video_upload\"]"
  },
  {
    "id":27,
    "title":"Portfolio",
    "url":"https://staging.littlelives.com/portfolios",
    "element":"//*[@id=\"btn_add_note\"]"
  },
  // {
  //   "id":null,
  //   "title":"Portfolio",
  //   "url":"https://staging.littlelives.com/portfolios",
  //   "element":"-"
  // },
  {
    "id":null,
    "title":"Portfolio - Album",
    "url":"https://staging.littlelives.com/portfolios/album/51b8096e-0f30-4fb6-b0d9-26a2740c3493",
    "element":"//*[@id=\"delete_link\"]"
  },
  {
    "id":25,
    "title":"Little Questions | LittleLives",
    "url":"https://staging.littlelives.com/questions",
    "element":"//*[@id=\"btnPostAnswer\"]"
  },
  {
    "id":26,
    "title":"Little Questions | LittleLives",
    "url":"https://staging.littlelives.com/questions",
    "element":"//*[@id=\"btnPostAnswer\"]"
  },
  {
    "id":9,
    "title":"Today to-dos",
    "url":"https://staging.littlelives.com/todos",
    "element":""
  },
 ];

casper.log(cases.length,'debug');


var login_url = 'http://192.168.1.55/users/login';
// var login_url = 'https://www.littlelives.com/users/login';
// var login_url = 'staging.littlelives.com/users/login';



casper.start(login_url, function() {
      this.test.assertExists('form[action="/users/login"]', 'form is found');
       this.fill('form[action="/users/login"]', {
          'data[User][username]': 'trongdan@littlelives.com',
          'data[User][password]':'littledan' },
          true);
       this.click('input[type="submit"]');
});

casper.waitForSelector('div[class="btn_set"]',function  () {
  var href = this.evaluate(function() {
        return document.location.href;
      });
  var title = this.evaluate(function() {
        return document.title;
      });
  console.log("Organisations href:  " + href + ' Title: ' +title);
});

// casper.thenEvaluate(function(){
//   console.log(document.title,'error');
//   this.echo(document.title);
// });

// casper.run();//.exit();

// casper.done();

// casper.then(function() {
  casper.eachThen(cases, function(response) {
    var url  = response.data.url.replace('staging.littlelives.com','192.168.1.55').replace('https','http');
    var t = response.data.title;
    var element = response.data.element;
    console.log('Opening '+url);
    // casper.log(title,'debug');
    this.thenOpen(url, function(res) {
      this.waitForSelector('div[class="page_main"]',function  () {
        var href = this.evaluate(function() {
              return document.location.href;
            });
        var title = this.evaluate(function() {
              return document.title;
            });
        // this.assertTitle(t);

        this.test.assertExists('div[class="page_main"]','Body is loaded');
        this.test.assertTitle(t);
         this.viewport(1024, 768);
         // var ACfilename = href.replace(/[^a-zA-Z0-9]/gi, '-').replace(/^https?-+/, '') + '/' + 1024 + '-' + 768 + ".png";
          //capture snaps a defined selection of the page
            //this.capture(ACfilename,{top: 0,left: 0,width: 1024, height: 768});
            //this.echo('snapshot taken');
            // this.capture(href+'.png');

        // console.log("Title expect: "  +t);
        // if(!!title) this.test.assertEquals(title,t);
        console.log("Href: " + href + ' Title: ' +title);
      },function () {
        console.log('Unable to load');
      });


      // // casper.log(url,'info');
      // var href = this.evaluate(function() {
      //   return document.location.href;//querySelector('h1').textContent;
      // });
      // casper.log(href,'info');
      // this.assertTitle(title);
      // casper.log('Title: '+title,'debug');

      // this.test.assertExists('form[action="/users/login"]', 'form is found');
      // search for 'casperjs' from google form
      // this.fill('form[action="/users/login"]', { 'data[User][username]': 'developers', 'data[User][password]':'1qazxsw2' }, true);
    });
  });
// });


casper.run();
// casper.done();
