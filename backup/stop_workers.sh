#!/bin/sh
#Purpose: Stop all the workers in beanstalk
#Usage  : sh stop_worker.sh
#app="/var/www/Littlelives/app"
app="/var/www/vhosts/littlelives.com/httpdocs/app"

# Thumbnail
i=1
max=10
while [ $i -lt $max ]
do
   echo "Stop Thumbnail Worker: w$i"
   $app/Console/cake thumbWorker w$i stop
   true $(( i++ ))
done
sleep 5

# Attendance
i=1
max=8
while [ $i -lt $max ]
do
   echo "Stop Attendance Worker: a$i"
   $app/Console/cake attdWorker a$i stop
   true $(( i++ ))
done
sleep 5

# Email
i=1
max=8
while [ $i -lt $max ]
do
   echo "Stop Email Worker: e$i"
   $app/Console/cake emailWorker e$i stop
   true $(( i++ ))
done
sleep 5

# s3Upload
i=1
max=8
while [ $i -lt $max ]
do
   echo "Stop s3Upload Worker: e$i"
   $app/Console/cake s3UploadWorker u$i stop
   true $(( i++ ))
done
sleep 5

# SMS
i=1
max=3
while [ $i -lt $max ]
do
   echo "Stop SMS Worker: e$i"
   $app/Console/cake smsWorker s$i stop
   true $(( i++ ))
done
