
var casper = require("casper").create(),
    viewportSizes = [
    // [320,480],
    // [320,568],
    // [600,1024],
    // [766,1024],
    [1024,1000]
    // [1280,800]
    // [1440,900]
],url,
    // url = casper.cli.get(0),
    saveDir = 'screenshots';//url.replace(/[^a-zA-Z0-9]/gi, '-').replace(/^https?-+/, '');

casper.start();
 
casper.each(viewportSizes, function(self, viewportSize, i) {
 
    // set two vars for the viewport height and width as we loop through each item in the viewport array
    var width = viewportSize[0],
        height = viewportSize[1],
        // host = 'https://littlelives.com',
        host = 'https://192.168.1.14',
        login_url = host+'/users/login';
        console.log(login_url);
        //give some time for the page to load

var fn_login = function() {
    var data;
    var role = 'twinkle';//casper.options.role;
    data = {
        'data[User][username]': 'trongdan@littlelives.com',
        'data[User][password]': 'littledan'
    };
    // console.log(this);
    this.test.assertExists('form[action="/users/login"]', 'login form is found');
    this.fill('form[action="/users/login"]', data, true);
    return this.click('input[type="submit"]');
};
    casper.start(login_url, fn_login);
    casper.wait(5000, function() {
 
        //set the viewport to the desired height and width
        this.viewport(width, height);
        // url  = 'http://www.littlelives.com/ndp';
        url = host+'/Portfolios/print_portfolio/73ca6628-9217-11e2-9fb0-c999a15ef2a4/26030';
        casper.thenOpen(url, function() {
            this.echo('Opening at ' + width);
 
            //Set up two vars, one for the fullpage save, one for the actual viewport save
            var FPfilename = saveDir + '/fullpage-' + width + ".png";
            var ACfilename = saveDir + '/' + width + '-' + height + ".png";
 
            //Capture selector captures the whole body
            this.captureSelector(FPfilename, 'body');
 
            //capture snaps a defined selection of the page
            this.capture(ACfilename,{
                top: 0,left: 0,width: width, height: height
            },{
                format: 'jpg',
                quality: 100
            });
            this.echo('snapshot taken');
        });
    });
});
 
casper.run(function() {
    this.echo('Finished captures for ' + url).exit();
});
