var casper = require('casper').create({
	verbose: false,
    logLevel: 'debug',
    pageSettings: {
         loadImages:  false,         // The WebPage instance used by Casper will
         loadPlugins: false,         // use these settings
         userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});


// print out all the messages in the headless browser context
casper.on('remote.message', function(msg) {
    this.echo('remote message caught: ' + msg);
});

// print out all the messages in the headless browser context
casper.on("page.error", function(msg, trace) {
    this.echo("Page Error: " + msg, "ERROR");
});


var url = 'https://littlelives.com/users/login';
// var url = 'https://192.168.1.55/users/login';

casper.start(url, function() {
	console.log('page loaded');
	this.test.assertExists('form[action="/users/login"]', 'form is found');

    // search for 'casperjs' from google form
    this.fill('form[action="/users/login"]', { 'data[User][username]': 'irene.teo@pcf.org.sg', 'data[User][password]':'littlelives' }, true);
});

casper.then(function(){
	var titleText = this.evaluate(function() {
        return document.title;//querySelector('h1').textContent;
    });
   console.log("Page Title " + titleText);
   // console.log("Your name is " + document.querySelector('.headerTinymanName').textContent ); 
});

casper.run();