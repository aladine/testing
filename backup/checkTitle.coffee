# cases = [
#   sn: 1
#   product: "Admin"
#   functionality: "Withdrawal of teacher/admin/student"
#   role: "Admin"
#   inputs: "\n1b. Principal to drag Teacher Liu Cui Xian OR Admin Katherine Wong from St. Hilda Kindergarten into the Withdrawn Room\n2. Student(Withdraw the student) - Reason: Family Issues 3. Last day: 06 July 2013\nTeacher/Admin(Quit Job!) - Reason: Family Issues 3. Last day: 06 July 2013"
#   procedure: "Teacher will drag and drop student into the Withdraw Room. State the reason and the date of withdrawal. Select withdraw."
#   description: "Withdrawal is successful"
#   username: "twinkle@littlelives.com"
#   title: "Admin"
#   url: "https://staging.littlelives.com/admin/organisations?h=22571f611a14d2fa3f0acd0f62dc0d39"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btn_dialog_withdraw\"]"
#   result_page: ""
#   tid: "AD1"
# ,
#   sn: 2
#   product: "Admin"
#   functionality: "Withdrawal of student"
#   role: "Teacher"
#   inputs: "1. Teacher Chetna Sachdev will Drag and drop student Aksh Venugopal Nair from class Raindrops (K1 AM) from St. Hilda Kindergarten into the Withdrawn Room 2. Reason: Family Issues 3. Last day: 06 July 2013"
#   procedure: "Teacher will drag and drop student into the Withdraw Room. State the reason and the date of withdrawal. Select withdraw."
#   description: "Withdrawal is successful"
#   username: "twinkle@littlelives.com"
#   title: "Admin"
#   url: "https://staging.littlelives.com/admin/organisations?h=22571f611a14d2fa3f0acd0f62dc0d39"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btn_dialog_withdraw\"]"
#   result_page: ""
#   tid: "AD2"
# ,
#   sn: 3
#   product: "Admin"
#   functionality: "Add Class"
#   role: "Principal/Admin"
#   inputs: "1. Name: K1\n2. Description: Morning session\n3. Centre: LittleLives Demo\n4. Level: Kindergarten 1\n5. Session: Morning"
#   procedure: "Select admin and select add class. Fill in the input fields for name, description, centre, level and session. Select save."
#   description: "Class K1(AM) will be added to LittleLives Demo"
#   username: "twinkle@littlelives.com"
#   title: "Add Class"
#   url: "https://staging.littlelives.com/admin/organisations/add_class?h=22571f611a14d2fa3f0acd0f62dc0d39"
#   exist: ""
#   first_click: ""
#   input: "//*[@id=\"password\"]: littlelives\ninput[name=\"user\"]: Rooney"
#   btn_submit: "//*[@id=\"btnSubmit\"]"
#   result_page: ""
#   tid: "AD3"
# ,
#   sn: 4
#   product: "Admin"
#   functionality: "Add Staff"
#   role: "Principal/Admin"
#   inputs: "1. Role: Administrator\n2. Email: siokping@littlelives.com\n3. Name: LittleLives - Siok Ping\n4. Title: Administrator\n5. Centre: LittleLives Demo\n6. Start Date: 6 July 2013\n7. Gender: Female"
#   procedure: "Select admin and select add staff. Fill in the input fields for role and email using a valid email. Select proceed. Fill in the input fields for name, title, centre, start date and gender. Select Save."
#   description: "Adding successful ! A welcome email and password will be send to the new user.Username:xxx . Password: [a meaningful password]"
#   username: "twinkle@littlelives.com"
#   title: "Add User"
#   url: "https://staging.littlelives.com/admin/organisations/add_user?h=22571f611a14d2fa3f0acd0f62dc0d39"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btnProceed\"] \n//*[@id=\"btnSave\"]"
#   result_page: ""
#   tid: "AD4"
# ,
#   sn: 5
#   product: "Admin"
#   functionality: "Add Staff"
#   role: "Principal/Admin"
#   inputs: "1. Role: Administrator\n2. Email: siokping@littlelives.com"
#   procedure: "Select admin and select add staff. Fill in the input fields for role and email using a valid email. Select proceed."
#   description: "Ooops! LittleLives-Siok Ping is already in LittleLives Demo."
#   username: "twinkle@littlelives.com"
#   title: ""
#   url: "https://staging.littlelives.com/admin/organisations/add_user?h=22571f611a14d2fa3f0acd0f62dc0d39"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btnProceed\"]"
#   result_page: ""
#   tid: "AD5"
# ,
#   sn: 6
#   product: "Admin"
#   functionality: "Add Staff"
#   role: "Principal/Admin"
#   inputs: "1. Role: Administrator\n2. Email: @littlelives.com"
#   procedure: "Select admin and select add staff. Fill in the input fields for role and email using a valid email. Select proceed."
#   description: "Ooops! Please enter a valid email address"
#   username: "twinkle@littlelives.com"
#   title: "Add People"
#   url: "https://staging.littlelives.com/admin/organisations/add_user?h=22571f611a14d2fa3f0acd0f62dc0d39"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//a[@id='btnProceed' and @class='btn_affirm']"
#   result_page: ""
#   tid: "AD6"
# ,
#   sn: 7
#   product: "Admin"
#   functionality: "Add Students"
#   role: "Principal/Admin/Teacher"
#   inputs: "1. Email or BC No.: (Valid Email or BC)\n2. Name: Jane\n3. Gender: Female\n4. Class: Demo Class 2(K1 PM)\n5. Start Date: 13 June 2013"
#   procedure: "Select admin and select add students. Fill in the input fields for email/BC No, Name, Gender, Class and Start Date. Select Submit."
#   description: ""
#   username: "twinkle@littlelives.com"
#   title: "Add Multiple Students"
#   url: "https://staging.littlelives.com/admin/organisations/add_students?h=22571f611a14d2fa3f0acd0f62dc0d39"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//a[@id='btnProceed' and @class='btn_affirm']"
#   result_page: ""
#   tid: "AD7"
# ,
#   sn: 8
#   product: "Admin"
#   functionality: "Transfering of student from one class to another"
#   role: "All"
#   inputs: "1. The start date is on (dd mm yyyy): 03 July 2013"
#   procedure: "Teacher:\nSelect admin and select class. Drag a student from one class to another class that she is teaching.\nAdmin/Principal: \nSelect admin and select class. Drag a student from one class to any other class."
#   description: "Teacher:\n\"Transfer Ang Xuan Ni to Sapphire 1(K1 AM) is successful!\" will be displayed.\n\nAdmin/Principal:\n\"Transfer Lek Zi Jie , Treven to Chrysolite 1(K1 AM) is successful!\" will be displayed"
#   username: "twinkle@littlelives.com"
#   title: "Admin"
#   url: "https://staging.littlelives.com/admin/organisations/add_students?h=22571f611a14d2fa3f0acd0f62dc0d39"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//button[@id='btn_dialog_transfer']"
#   result_page: ""
#   tid: "AD8"
# ,
#   sn: 9
#   product: "Admin"
#   functionality: "Weight and Height"
#   role: "Teacher/Admin"
#   inputs: "1. Semester1: H 130cm W 30kg BMI 17.75"
#   procedure: "Select Admin and then select Weight and Height tab. Fill in the input field for weight and height. Select save."
#   description: "Check that BMI is calculated (Weight [kg] divided by height [m] squared)\n\"Weight and Height are saved!\" will be displayed\n"
#   username: "twinkle@littlelives.com"
#   title: "Admin"
#   url: "https://staging.littlelives.com/admin/organisations/add_students?h=22571f611a14d2fa3f0acd0f62dc0d39"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "a[@class='btn_affirm btn_save_wh']"
#   result_page: ""
#   tid: "AD9"
# ,
#   sn: 10
#   product: "Admin"
#   functionality: "Adding multiple students at one go"
#   role: "Principal/Admin/teachers"
#   inputs: "BC: S9000551J\nName:Zee\nGender:Girl\nClass: (NN)\nStart Date: 25 Jun 2013\n\nBC: S9105399C\nName:SP\nGender:Girl\nClass: -(N1 PM)\nStart Date: 25 Jun 2013\n\nEmail: zee@littlelives.com\nName: zzz\nGender:Boy\nClass: 1 (K1 PM)\nStart Date: 25 Jun 2013\n\nEmail: siokping@littlelives.com\nName: Siok\nGender:Boy\nClass: 1 (K2 PM)\nStart Date: 25 Jun 2013\n\nEmail: zeemonster90@gmail.com\nName: zeeeee\nGender: Girl\nClass: 1 (K1 PM)\nStart Date: 25 Jun 2013"
#   procedure: "Using evangel kindergarten, go to Admin and select Add Students. Add 5 students and select Submit."
#   description: "Once submitted, page will be redirected to Admin page. Students that are added to that particular class with the start date of today will appear on the class list."
#   username: "twinkle@littlelives.com"
#   title: "Add Multiple Students"
#   url: "https://staging.littlelives.com/admin/organisations/add_students"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//a[@id='btnProceed' and @class='btn_affirm']"
#   result_page: ""
#   tid: "AD10"
# ,
#   sn: 11
#   product: "Admin"
#   functionality: "Parent's Communication"
#   role: "Principal/Admin"
#   inputs: "Name: SP\nRelationship: Guardian\nEmail: siokping@littlelives.com\nMobile: 92968847\n\nRecipients: 92968847\nMessage: Hi"
#   procedure: "Using evangel kindergarten, go to Parent's Communication. Select SMS then select Parent to edit Parent information and save it. Change the mobile to your own phone number. Check only that student and select compose sms."
#   description: "Once parent's information is edited, \"Edit parent's information successful!\" is displayed. Once SMS is sent, \"Message(SMS) will be sent to parents of students in Amethyst 1 (K2 AM). 9296884\" will be displayed in the Principal/Admin notification."
#   username: "twinkle@littlelives.com"
#   title: "Admin"
#   url: "https://staging.littlelives.com/admin/organisations"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "/html/body/div/div[3]/div[2]/div/section/div[2]/table/tfoot/tr/td/a\n//*[@id=\"btn_send_sms\"]"
#   result_page: ""
#   tid: "AD11"
# ,
#   sn: 12
#   product: "Admin"
#   functionality: "Add Student who belongs to an existing class to another class"
#   role: "Principal/Admin"
#   inputs: "Email: zeemonster90@gmail.com\nName: zeeeee\nGender: Girl\nClass: Demo Class 2 第二班 (K1 PM)\nStart Date: 1 Jun 2013\n\nEmail: zeemonster90@gmail.com\nName: zeeeee\nGender: Girl\nClass: 1 (K1 PM)\nStart Date: 25 Jun 2013"
#   procedure: "Add student to class and then try adding the same student again to another class in another school."
#   description: "\"Hi Principal, Student zeee (S9000551J) has joined a new class. Please withdraw him/her from previous class Demo Class 2 第二班 (K1 PM).\nThank you!\" will be displayed."
#   username: "twinkle@littlelives.com"
#   title: "Add students"
#   url: "https://staging.littlelives.com/admin/organisations/add_students?h=1c81d1f044c7cea35d1c8f29f7f9f992"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btnProceed\"]"
#   result_page: ""
#   tid: "AD12"
# ,
#   sn: 13
#   product: "Attendance"
#   functionality: "Attendance taking of a class"
#   role: "Teacher"
#   inputs: "\n1. Go to 2nd July 2013\n"
#   procedure: "Go to a non-term break date. Take attendance for the entire class. Select save."
#   description: "23 present and 1 absent"
#   username: "twinkle@littlelives.com"
#   title: "Cheerfulness"
#   url: "https://staging.littlelives.com/attendances/roster/26600/20130702"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btnSave\"]"
#   result_page: ""
#   tid: "AT13"
# ,
#   sn: 14
#   product: "Attendance"
#   functionality: "Attendance taking of a staff"
#   role: "Principal"
#   inputs: "1. Go to 2nd July 2013\n"
#   procedure: "Go to a non-term break date. Take attendance for the entire class. Select save."
#   description: "23 present and 1 absent"
#   username: "dotdad@littlelives.com"
#   title: "St.Hilda's Kindergarten"
#   url: "https://staging.littlelives.com/attendances/roster/26537/20130702"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btnEnhancedSave\"]"
#   result_page: ""
#   tid: "AT14"
# ,
#   sn: 15
#   product: "Attendance"
#   functionality: "Adding remarks when marking attendance"
#   role: "Principal/Admin/Teacher"
#   inputs: "\n"
#   procedure: "Select attendance and select remarks.\nSelect the the necessary options."
#   description: "Reasons selected will appear in the remarks column."
#   username: "twinkle@littlelives.com"
#   title: "St.Hilda's Kindergarten"
#   url: "https://staging.littlelives.com/attendances/roster/26537/20130702"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "/html/body/div[2]/div[11]/div/button[2]"
#   result_page: ""
#   tid: "AT15"
# ,
#   sn: 16
#   product: "Attendance"
#   functionality: "Declare day off for kindergarten"
#   role: "Principal"
#   inputs: "1. Select 5th July 2013\n2. Declare day off for St. Hilda Kindergarten\n2. Reason: Bird Flu breakout"
#   procedure: "Select Kindergarten and declare day off for Kindergarten. Write down the reason and select ok."
#   description: "On 5th July all classes will have a day off with the with the reason(bird flu) stated below."
#   username: "dotdad@littlelives.com"
#   title: "Attendance"
#   url: "https://staging.littlelives.com/attendances/index/20130705"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "/html/body/div[2]/div[3]/div/button[2]"
#   result_page: ""
#   tid: "AT16"
# ,
#   sn: 17
#   product: "Attendance"
#   functionality: "Cancel day off for kindergarten"
#   role: "Principal"
#   inputs: "1. Select 5th July 2013\n2. Cancel day off for St. Hilda Kindergarten\n"
#   procedure: "Select Kindergarten and cancel day off for Kindergarten."
#   description: "On 5th July all classes will resume and attendance needs to be taken."
#   username: "dotdad@littlelives.com"
#   title: "Attendance"
#   url: "https://staging.littlelives.com/attendances/index/20130705"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "/html/body/div[2]/div/div[2]/div/div/p/a"
#   result_page: ""
#   tid: "AT17"
# ,
#   sn: 18
#   product: "Login"
#   functionality: "Logging in Users"
#   role: "All"
#   inputs: "1. Username: twinkle@littlelives.com\n2. Password: littlelives"
#   procedure: "Put username into username input field and pwd into password input field. Submit."
#   description: "User will be redirected to the homepage that will display today's to-dos"
#   username: "twinkle@littlelives.com"
#   title: "LittleLives"
#   url: "https://staging.littlelives.com/users/login"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "/html/body/div/div/form/div[2]/fieldset/div[3]/input"
#   result_page: ""
#   tid: "LO18"
# ,
#   sn: 19
#   product: "Login"
#   functionality: "Logging in Users"
#   role: "All"
#   inputs: "1. Username: twinkle@littlelives.com\n2. Password: 12345"
#   procedure: "Put username into username input field and invalid pwd into password input field. Submit."
#   description: "User will be brought to the login page again and error message of \"incorrect password, try again\" will be displayed"
#   username: "twinkle@littlelives.com"
#   title: "LittleLives"
#   url: "https://staging.littlelives.com/users/login"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "/html/body/div/div/form/div[2]/fieldset/div[3]/input"
#   result_page: ""
#   tid: "LO19"
# ,
#   sn: 20
#   product: "Login"
#   functionality: "Logging in Users"
#   role: "All"
#   inputs: "1. Username: (empty)\n2. Password: littlelives"
#   procedure: "Enter a valid password into password input field leaving the username input field empty"
#   description: "Username will prompt : \"This is a required field\""
#   username: "twinkle@littlelives.com"
#   title: "LittleLives"
#   url: "https://staging.littlelives.com/users/login"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "/html/body/div/div/form/div[2]/fieldset/div[3]/input"
#   result_page: ""
#   tid: "LO20"
# ,
#   sn: 21
#   product: "Login"
#   functionality: "Logging in Users"
#   role: "All"
#   inputs: "1. Username: (empty)\n2. Password: (empty)"
#   procedure: "Leave username and password input field empty"
#   description: "Username will prompt : \"This is a required field\""
#   username: "twinkle@littlelives.com"
#   title: "LittleLives"
#   url: "https://staging.littlelives.com/users/login"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "/html/body/div/div/form/div[2]/fieldset/div[3]/input"
#   result_page: ""
#   tid: "LO21"
# ,
#   sn: 22
#   product: "Login"
#   functionality: "Logging in Users"
#   role: "All"
#   inputs: "1. Username: ll@littlelives.com 2. Password: littlelives"
#   procedure: "Enter an invalid username into the username input field with a correct password"
#   description: "Invalid username, try again"
#   username: "twinkle@littlelives.com"
#   title: "LittleLives"
#   url: "https://staging.littlelives.com/users/login"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "/html/body/div/div/form/div[2]/fieldset/div[3]/input"
#   result_page: ""
#   tid: "LO22"
# ,
#   sn: 23
#   product: "Login"
#   functionality: "Retrieve forgotten password"
#   role: "All"
#   inputs: "1. Username: (any personal email)@littlelives.com"
#   procedure: "Put valid username into username input field. Submit."
#   description: "User will be redirected to the success page that mention that email has been sent"
#   username: "twinkle@littlelives.com"
#   title: "LittleLives"
#   url: "https://staging.littlelives.com/users/forgetpassword"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "/html/body/div/div/form/div[2]/fieldset/div[2]/input"
#   result_page: ""
#   tid: "LO23"
# ,
#   sn: 24
#   product: "Login"
#   functionality: "Retrieve forgotten password"
#   role: "All"
#   inputs: "1. Username: rubbish@littlelives.com"
#   procedure: "Put invalid username into username input field. Submit."
#   description: "User will be directed to the retrieve password page with an error message \"Sorry, we could not find your email or username associated with any LittleLives account. Please try again with correct email or username, Contact us at feedback@littlelives.com for further assistance.\""
#   username: "twinkle@littlelives.com"
#   title: "LittleLives"
#   url: "https://staging.littlelives.com/users/forgetpassword"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "/html/body/div/div/form/div[2]/fieldset/div[2]/input"
#   result_page: ""
#   tid: "LO24"
# ,
#   sn: 25
#   product: "Login"
#   functionality: "Retrieve forgotten password"
#   role: "All"
#   inputs: "1. Username: dotdad@littlelives.com"
#   procedure: "Put valid username into username input field. Submit."
#   description: "User will be directed to the retrieve password page with message \"Check your email!\nYou will receive an email from us with the new password.\nIf you don't receive an email from us, check your spam filter or contact us at feedback@littlelives.com for further assistance.\""
#   username: "twinkle@littlelives.com"
#   title: "LittleLives"
#   url: "https://staging.littlelives.com/users/forgetpassword"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "/html/body/div/div/form/div[2]/fieldset/div[2]/input"
#   result_page: ""
#   tid: "LO25"
# ,
#   sn: 26
#   product: "Notifications"
#   functionality: "Withdrawal of student"
#   role: "Principal/Admin/Teacher"
#   inputs: ""
#   procedure: "View notifications"
#   description: "Notification from Chetna Sachdev at 10.11am today \"Hi there! Please note that I have withdrawn student Aksh Venugopal Nair (T1007203J) from class Raindrops (N1 AM).\" will be displayed on the Notifications tab to the admins and the teacher teaching that particular class."
#   username: "twinkle@littlelives.com"
#   title: "Notifications"
#   url: "https://staging.littlelives.com/notifications?h=12ef50fd988896cc15aacb4028182c08"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: ""
#   result_page: ""
#   tid: "NO26"
# ,
#   sn: 27
#   product: "Notifications"
#   functionality: "Attendance taking of a class"
#   role: "Principal/Admin"
#   inputs: ""
#   procedure: "View notifications"
#   description: "Notification from Adaikalasamy Bernadette at 10.54am today \"Attendance for class Cheerfulness on 02-07-2013 has been taken\" will be displayed on the Notifications tab."
#   username: "twinkle@littlelives.com"
#   title: "Notifications"
#   url: "https://staging.littlelives.com/notifications?h=12ef50fd988896cc15aacb4028182c08"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: ""
#   result_page: ""
#   tid: "NO27"
# ,
#   sn: 28
#   product: "Notifications"
#   functionality: "Attendance taking of a staff"
#   role: "Admin/Teacher"
#   inputs: ""
#   procedure: "View notifications"
#   description: "Notification from Adaikalasamy Bernadette at 10.54am today \"Staff Attendance for St. Hilda's Kindergarten on 02-07-2013 has been taken\" will be displayed on the Notifications tab."
#   username: "twinkle@littlelives.com"
#   title: "Notifications"
#   url: "https://staging.littlelives.com/notifications?h=12ef50fd988896cc15aacb4028182c08"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: ""
#   result_page: ""
#   tid: "NO28"
# ,
#   sn: 29
#   product: "Notifications"
#   functionality: "Transfering of student from one class to another"
#   role: "Principal/Admin/Teacher"
#   inputs: "1.Drag Student Caleb Teo Shun Kai from class Rainbow(N1 AM) to class Sunshine (N1 AM)."
#   procedure: "Select Admin. Drag and Drop to transfer a student from one class to another class."
#   description: "Nofication from Principal Sheila Cynthia \" I have transferred student Caleb Teo Shun Kai (T1007449A) from class Rainbow (N1 PM) to class Sunshine (N1 AM).\" and \"I have added student Caleb Teo Shun Kai to class Diamond 1 (N1 AM) will be displayed"
#   username: "twinkle@littlelives.com"
#   title: "Admin"
#   url: "https://staging.littlelives.com/admin/organisations?h=12ef50fd988896cc15aacb4028182c08"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btn_dialog_transfer\"]"
#   result_page: ""
#   tid: "NO29"
# ,
#   sn: 30
#   product: "Notifications"
#   functionality: "Transfering of student from one school to another"
#   role: "Principal"
#   inputs: "N.A"
#   procedure: "Log in as admin. Drag and drop student from one school to another."
#   description: "Principal from both schools will receive this message. \"Hi Principal, Student Au Ze Rui Ethan...has joined a new class. Please withdraw him/her from previous class Brilli 1(K1 AM). Should you have any question, please contact the other centre at 6569-4373. Thankyou!\""
#   username: "dotdad@littlelives.com"
#   title: "Admin"
#   url: "https://staging.littlelives.com/admin/organisations"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btn_dialog_transfer\"]"
#   result_page: ""
#   tid: "NO30"
# ,
#   sn: 31
#   product: "Portfolio"
#   functionality: "Add Photo"
#   role: "Teacher/Admin/Principal"
#   inputs: "1. ChooseClass: LittleLives 1A K1\n2. Choose Photo: (Any 2 pictures)\n3. Choose Album: Office Monday\n4. Caption: (Enter something)"
#   procedure: "Select portfolio and select add photo.\nSelect a class and a photo(max 5). Select Upload. Once uploaded, select next. Choose an album and select next. Enter caption and select done."
#   description: "Photos in the album(Office Monday) are being uploaded to the portfolio"
#   username: "twinkle@littlelives.com"
#   title: "Add Photos"
#   url: "https://staging.littlelives.com/portfolios?h=2253652703d3c4f6e6d977635c25277e"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btn_photos_upload\"]"
#   result_page: ""
#   tid: "PO31"
# ,
#   sn: 32
#   product: "Portfolio"
#   functionality: "Add Video"
#   role: "Teacher/Admin/Principal"
#   inputs: "1. ChooseClass: LittleLives 1A K1\n2. Choose Photo: (Any photo)"
#   procedure: "Select portfolio and select add video.\nSelect a class and a photo. Select Upload. Choose an album and select next. Enter caption and select done."
#   description: "Please choose a video type"
#   username: "twinkle@littlelives.com"
#   title: "Add Videos"
#   url: "https://staging.littlelives.com/portfolios?h=2253652703d3c4f6e6d977635c25277e"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btn_video_upload\"]"
#   result_page: ""
#   tid: "PO32"
# ,
#   sn: 33
#   product: "Portfolio"
#   functionality: "Add Video"
#   role: "Teacher/Admin/Principal"
#   inputs: "1. ChooseClass: LittleLives 1A K1\n2. Choose Photo: (Any video)"
#   procedure: "Select portfolio and select add video.\nSelect a class and a video. Select Upload. Once uploaded, select next"
#   description: "Video in the album(Office Monday) is being uploaded to the portfolio"
#   username: "twinkle@littlelives.com"
#   title: "Add Videos"
#   url: "https://staging.littlelives.com/portfolios?h=2253652703d3c4f6e6d977635c25277e"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btn_video_upload\"]"
#   result_page: ""
#   tid: "PO33"
# ,
#   sn: 34
#   product: "Portfolio"
#   functionality: "Evaluation"
#   role: "Teacher"
#   inputs: ""
#   procedure: "Select Portfolio and then select evaluation. Select Term 1 and evaluate the options and select save evaluation"
#   description: "Evaluation has been saved!"
#   username: "twinkle@littlelives.com"
#   title: "Portfolio"
#   url: "https://staging.littlelives.com/portfolios?h=2253652703d3c4f6e6d977635c25277e"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btn_add_note\"]"
#   result_page: ""
#   tid: "PO34"
# ,
#   sn: 35
#   product: "Portfolio"
#   functionality: "Lazy loading on Timeline"
#   role: "All"
#   inputs: "N.A."
#   procedure: "Go to Portfolio page;\nScroll the timeline page till the bottom;"
#   description: "1. If there are more than 10 moments, you will be able to see loading bar and more moments be loaded after a while;\n2. If you see [Beginning] tag at the end of timeline, you should not be able to view more moment and there should NOT be any loading bar appear."
#   username: "twinkle@littlelives.com"
#   title: "Portfolio"
#   url: "https://staging.littlelives.com/portfolios?h=2253652703d3c4f6e6d977635c25277e"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "-"
#   result_page: ""
#   tid: "PO35"
# ,
#   sn: 36
#   product: "Portfolio"
#   functionality: "Timeline shows moments based on permission"
#   role: "Teacher/Principal/High-level User"
#   inputs: "N.A."
#   procedure: "Log in as a teacher/principal/high-level user;\nGo to Portfolio page;\nView Portfolio Timeline."
#   description: "1. If you are a teacher, you should be able to see moments in YOUR CLASS ONLY.\n2. If you are a principal, you should be able to see moments in YOUR CENTRE ONLY.\n3. If you are a LittleLives Admin, you should be able to see ALL the moments. You should be able to navigate between different centres/classes by clicking the tree."
#   username: "twinkle@littlelives.com"
#   title: "Portfolio"
#   url: "-"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "-"
#   result_page: ""
#   tid: "PO36"
# ,
#   sn: 37
#   product: "Portfolio"
#   functionality: "Lightbox: View Photos"
#   role: ""
#   inputs: ""
#   procedure: ""
#   description: ""
#   username: "twinkle@littlelives.com"
#   title: "Portfolio"
#   url: "-"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "-"
#   result_page: ""
#   tid: "PO37"
# ,
#   sn: 38
#   product: "Portfolio"
#   functionality: "Lightbox: View Videos"
#   role: ""
#   inputs: ""
#   procedure: ""
#   description: ""
#   username: "twinkle@littlelives.com"
#   title: "Portfolio"
#   url: "-"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "-"
#   result_page: ""
#   tid: "PO38"
# ,
#   sn: 39
#   product: "Portfolio"
#   functionality: "Delete Album"
#   role: "Teacher"
#   inputs: "N.A"
#   procedure: "Select Portfolio and select view albums.\nSelect an album and select delete this album. Select ok."
#   description: "Album is deleted."
#   username: "twinkle@littlelives.com"
#   title: "Portfolio - Album"
#   url: "https://staging.littlelives.com/portfolios/album/51b8096e-0f30-4fb6-b0d9-26a2740c3493"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"delete_link\"]"
#   result_page: ""
#   tid: "PO39"
# ,
#   sn: 40
#   product: "Questions"
#   functionality: "Post an answer"
#   role: "Teacher/Admin/Principal/ Student"
#   inputs: "1.(Write an answer)"
#   procedure: "Write an answer and select a photo. Select share my answer."
#   description: "Your answer has been posted"
#   username: "twinkle@littlelives.com"
#   title: "Little Questions | LittleLives"
#   url: "https://staging.littlelives.com/questions"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btnPostAnswer\"]"
#   result_page: ""
#   tid: "QU40"
# ,
#   sn: 41
#   product: "Questions"
#   functionality: "Post an answer"
#   role: "Teacher/Admin/Principal/ Student"
#   inputs: "1.(empty)"
#   procedure: "Leave the answer input field empty. Select share my answer."
#   description: "Your answer cannot be empty"
#   username: "twinkle@littlelives.com"
#   title: "Little Questions | LittleLives"
#   url: "https://staging.littlelives.com/questions"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: "//*[@id=\"btnPostAnswer\"]"
#   result_page: ""
#   tid: "QU41"
# ,
#   sn: 42
#   product: "To-dos"
#   functionality: "Clearing to-dos"
#   role: "All"
#   inputs: "1. To-do: \"Hi Teacher! Please remember to take attendance for your class Tea Party 2013 (NN AM)\""
#   procedure: "Select a To-do and take attendance for that class. Select save."
#   description: "To-do: \"Hi Teacher! Please remember to take attendance for your class Tea Party 2013 (NN AM)\" should be remove from Today To-dos list once to-do is completed"
#   username: "twinkle@littlelives.com"
#   title: "Tasks"
#   url: "https://staging.littlelives.com/todos?h=23c4bd1afe15f6615c756f7045896608"
#   exist: ""
#   first_click: ""
#   input: ""
#   btn_submit: ""
#   result_page: ""
#   tid: "TO42"
# ]

casper = require("casper").create(
  verbose: false
  logLevel: "debug"
  pageSettings:
    loadImages: false
    loadPlugins: false
    userName: 'developers'
    password: '1qazxsw2'
    # webSecurityEnabled: false
    userAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4"
  )

x = require('casper').selectXPath
u = require('utils')
f = require('fs')
# __utils__.echo 'df'
# casper.on "remote.message", (msg) ->
  # @echo "Remote message caught: " + msg
casper.on 'remote.message', (msg) ->
	# @echo "Remote messge #{msg}"

casper.on 'page.error', (msg, trace) ->
	@echo "Page Error #{msg}","ERROR"

casper.on "http.status.200", (resource) ->
    @echo "#{resource.url} is OK", "INFO"

casper.on "http.status.301", (resource) ->
    @echo "#{resource.url} is permanently redirected", "PARAMETER"

casper.on "http.status.302", (resource) ->
    @echo "#{resource.url} is temporarily redirected", "PARAMETER"

casper.on "http.status.404", (resource) ->
    @echo "#{resource.url} is not found", "COMMENT"

casper.on "http.status.500", (resource) ->
    @echo "#{resource.url} is in error", "ERROR"

# e.url = e.url.replace("staging.littlelives.com", "192.168.1.55").replace("https", "http")
stream = fs.open './Attendance.json', 'r'
cases = stream.read()
# u.dump cases
# obj
try
  cases = JSON.parse cases
  # u.dump obj
catch e
  console.error e



# cases = fs.read('testcases.json')
# str = JSON.stringify cases, null ,'  '
# cases = JSON.parse '{'+cases+'}'
# u.dump cases
# login_url = 'http://192.168.1.55/users/login';
login_url = 'https://staging.littlelives.com/users/login'
# login_url = 'http://www.google.com'
tm = new Date()
tm = tm.getFullYear() + "_" + (tm.getMonth() + 1) + "_" + tm.getDate() + "_" + tm.getHours() + "_" + tm.getMinutes()
casper.snap = (tid)->
  n = new Date()
  @viewport 1024, 768
  @capture "#{tm}/screenshot_#{tid}.png",
    top: 0
    left: 0
    width: 1024 
    height: 768
  @echo "Snapshot done at "+ n.getHours()+':'+n.getMinutes()+':'+n.getSeconds()+" Link: 192.168.2.20:8000/screenshot_#{tid}.png" 

console.log login_url


casper.start login_url, ->
    # login to system first
    # @snap 0
    data = 
    	'data[User][username]': 'trongdan@littlelives.com'
    	'data[User][password]':'littledan' 
    @test.assertExists 'form[action="/users/login"]', 'login form is found'
    @fill 'form[action="/users/login"]', data, true	
    @click 'input[type="submit"]'

casper.eachThen cases, (response) ->
  url = response.data.url
  t = response.data.title
  element = response.data.exist
  id = response.data.sn 
  tid = response.data.tid 

  console.log "-------------------"
  console.log "Test case #{tid}"
  console.log "Product: "+response.data.product
  console.log "Functionality: "+ response.data.functionality
  console.log "Opening " + url

  @thenOpen url, (res) ->
    # @waitForSelector "div[class=\"page_main\"]", (->
    href = @evaluate(->
      document.location.href
    )
    title = @evaluate(->
      document.title
    )
    @test.assertExists "div[class=\"page_wrapper\"]", "Body is loaded"
    @test.assertTitle t
    # @test.assertMatchUrl

    if u.isArray element 
      Array::map.call element, (e) ->
        casper.test.assertExists x(e), "Element exists ->"+x(e)
    else @test.assertExists x(element), "Element exists ->"+x(element) if element isnt "" 
    
    # @snap tid

casper.run()
