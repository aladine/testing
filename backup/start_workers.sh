#!/bin/sh
#Purpose: Start all the workers in beanstalk
#Usage  : sh start_worker.sh
#app="/var/www/Littlelives/app"
app="/var/www/vhosts/littlelives.com/httpdocs/app"

# Thumbnail
i=1
max=10
while [ $i -lt $max ]
do
   echo "Start Thumbnail Worker: w$i"
   nohup $app/Console/cake thumbWorker w$i start < /dev/null &
   true $(( i++ ))
done
sleep 5

# Attendance
i=1
max=8
while [ $i -lt $max ]
do
   echo "Start Attendance Worker: a$i"
   nohup $app/Console/cake attdWorker a$i start < /dev/null &
   true $(( i++ ))
done
sleep 5

# Email
i=1
max=8
while [ $i -lt $max ]
do
   echo "Start Email Worker: e$i"
   nohup $app/Console/cake emailWorker e$i start < /dev/null &
   true $(( i++ ))
done
sleep 5

# s3Upload
i=1
max=8
while [ $i -lt $max ]
do
   echo "Start s3Upload Worker: u$i"
   nohup $app/Console/cake s3UploadWorker u$i start < /dev/null &
   true $(( i++ ))
done
sleep 5

# SMS
i=1
max=3
while [ $i -lt $max ]
do
   echo "Start SMS Worker: e$i"
   nohup $app/Console/cake smsWorker s$i start < /dev/null &
   true $(( i++ ))
done
