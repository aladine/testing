#!/bin/sh
#Purpose: Stop all the workers in beanstalk
#Usage  : sh stop_worker.sh
#app="/var/www/Littlelives/app"
app="/var/www/vhosts/littlelives.com/httpdocs/app"
worker_name="$1"
fn_stop(){
	local i=1
	local max="$1"
	local worker="$2"
	local prefix="$3"
	while [ $i -lt $max ]
	do
	   echo "Stop $worker: $prefix$i"
	   $app/Console/cake $worker $prefix$i stop
	   true $(( i++ ))
	done
	sleep 5
}

case "$1" in
    "thumbWorker" )
		fn_stop 10 $worker_name w
		exit 0
		;;
	"attdWorker" )
		fn_stop 8 $worker_name a
		exit 0
		;;
	"emailWorker" )			
		fn_stop 8 $worker_name e
		exit 0
		;;
	"s3UploadWorker" )
		fn_stop 8 $worker_name u
		exit 0
		;;
	"smsWorker" ) 
		fn_stop 3 $worker_name s
		exit 0
		;;
esac	

if [[ "$1" = "all" ]]; then
	fn_stop 10 "thumbWorker" w
	fn_stop 8 "attdWorker" a
	fn_stop 8 "emailWorker" e
	fn_stop 8 "s3UploadWorker" u
	fn_stop 3 "smsWorker" s
	exit 0
fi	