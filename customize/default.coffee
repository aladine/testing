# casper = require("casper").create(
#   verbose: true
#   # logLevel: "debug"
  # pageSettings:
  #   loadImages: false
  #   loadPlugins: false
  #   userName: 'developers'
  #   password: '1qazxsw2'
  #   # webSecurityEnabled: false
  #   userAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4"
  # )

casper.on 'remote.message', (msg) ->
  # @echo "Remote messge #{msg}"

casper.on 'page.error', (msg, trace) ->
  @echo "Page Error #{msg}","ERROR"

casper.on "http.status.200", (resource) ->
  @echo "#{resource.url} is OK", "INFO"

casper.on "http.status.301", (resource) ->
  @echo "#{resource.url} is permanently redirected", "PARAMETER"

casper.on "http.status.302", (resource) ->
  @echo "#{resource.url} is temporarily redirected", "PARAMETER"

casper.on "http.status.404", (resource) ->
  @echo "#{resource.url} is not found", "COMMENT"

casper.on "http.status.500", (resource) ->
  @echo "#{resource.url} is in error", "ERROR"

casper.test.on "fail", (failure) ->
  console.error 'ERROR'
  sc = "/errors/"+casper.getCurrentTimestamp()+'.png'
  # console.warn dir+sc
  casper.capture casper.options.casperDir+sc
  console.warn 'Link: '
  console.warn casper.options.casperLink+sc
  # casper.exit 1

casper.options.pageSettings["loadImages"]= false
casper.options.pageSettings["loadPlugins"]= false
casper.options.pageSettings[ "userName"]= "developers"
casper.options.pageSettings["password"]= "1qazxsw2"
casper.options.casperLink = 'https://dl.dropboxusercontent.com/u/49259543/casper' 
casper.options.casperDir = '/Users/littlelives/Dropbox/Public/casper'
# l = 'https://dl.dropboxusercontent.com/u/22391606/casper'  
# u.dump casper.options.pageSettings

casper.accounts = 
  twinkle:
    username: "twinkle@littlelives.com"
    password: "littlelives"
  tchr:
    username: "pcf-buvanesh"
    password: "littlelives"
  owner:
    username: "padma@shkindy.com"
    password: "littlelives"
  prnt:
    username: "leeminhoo@gmail.com"
    password: "littlelives"
  superadmin:
    username: "trongdan@littlelives.com"
    password: "littledan"
  stdnt:
    username: "zee@primary.sch"
    password: "littlelives"

# casper.assertAndClick = (test, e)->
#   test.assertExists e
#   @click e

casper.convertLocalUrl = (e)->
  e.replace("staging.littlelives.com", "192.168.1.55").replace("https", "http")
casper.getCurrentTimestamp = ()->
  n = new Date()
  n.getHours()+':'+n.getMinutes()+':'+n.getSeconds()