# casperjs --config=./config/config.json --includes=./customize/default.coffee --host=https://staging.littlelives.com --minicase=3 test ./customize/attendance.coffee
# casperjs --config=./config/config.json --includes=./customize/default.coffee --host=https://192.168.2.27 --minicase=5 test ./customize/attendance.coffee
# options: role, 
#INIT
x = require('casper').selectXPath
u = require('utils')

host   = casper.cli.get 'host'
# screenshot = casper.cli.get "screenfile"
# casper
#   .log("Using testhost: #{testhost}", "info")
#   .log("Using screenshot: #{screenshot}", "info")
# # Capture screens from all fails
# casper.test.on "fail", (failure) ->
#   casper.capture(screenshot) 
#   casper.exit 1

# # Capture screens from timeouts from e.g. @waitUntilVisible
# # Requires RC3 or higher.
# casper.options.onWaitTimeout = ->
#   @capture(screenshot)
#   @exit 1
casper.options.pageSettings["loadImages"]= true
casper.options.pageSettings["loadPlugins"]= true

tm = new Date()
tm = tm.getFullYear() + "_" + (tm.getMonth() + 1) + "_" + tm.getDate() + "_" + tm.getHours() + "_" + tm.getMinutes()
counter = 1
fn_snap = (tid)->
  n = new Date()
  # dir = 'casper'#'/Users/dan/Dropbox/Public/casper'
  # l = 'casper'#'https://dl.dropboxusercontent.com/u/22391606/casper'
  if typeof(tid) is "string" or typeof(tid) is "number"
    sc = "/screenshots/#{tm}_#{tid}.png"
  else
    sc = "/screenshots/#{tm}_#{counter++}.png"
  casper.viewport 1024, 768
  casper.capture casper.options.casperDir+sc,
    top: 0
    left: 0
    width: 1024 
    height: 768
  console.log "Snapshot: "
  console.log "#{casper.options.casperLink+sc}"
  console.log "Done at "+ casper.getCurrentTimestamp()

fn_reset = ->
  @test.comment "Reset Attendance" #RESET ATTENDANCE
  @click xid "resetAttendance"
  @waitForSelector '.ui-dialog-buttonset button.btn_affirm', ->
    @click xid "btn_done_reset_attendance"
    @reload ->
      @then fn_snap 
  
fn_save = ->
  #click save
  # @test.assertExists "#btnEnhancedSave"
  @click '#btnEnhancedSave'
  @waitForSelectorTextChange "#btnEnhancedSave", ->
    @test.comment "Save successful!"
    fn_snap()
#VARIABLE
# host = 'https://staging.littlelives.com'
# host = 'http://192.168.1.55'
login_url = host+'/users/login' 
count = 0
testcase = 
  title: "Cheerfulness"
  url: host+"/attendances/roster/26600/20130703"

# in test mode, get params from options in Grunt.js file, otherwise get from arguments
# if casper.options.test
#   role = casper.options["role"] or 'twinkle'
#   minicase = casper.options["minicase"] or "0"
# else 
#   role = casper.cli.get("role") or 'twinkle' 

minicase = casper.cli.get("minicase") or 0
role = 'superadmin'
# u.dump role
# minicase=5
# u.dump minicase==1
# u.dump casper.options.role
# casper.exit 1
total_cases=
  0:2
  1:3
  2:3
  3:4
  4:4
  5:3

#GET THE ELEMENT WITH ID IN DOM   
xid = (id)->
  x "//*[@id=\"#{id}\"]"

casper.test.begin "ATTENDANCE TESTCASE", total_cases[minicase], suite = (test) ->  
  #LOGIN TO STAGING
  casper.start login_url, ->
    data =  
      'data[User][username]': casper.accounts[role].username
      'data[User][password]': casper.accounts[role].password
    @waitForSelector 'form[action="/users/login"]', ->
      test.assertExists 'form[action="/users/login"]', 'login form is found'
      @fill 'form[action="/users/login"]', data, true
      @click 'input[type="submit"]'
  casper.thenOpen testcase.url, (res) ->
    @waitForSelector "div[class=\"page_main\"]", ->
      test.assertExists "div[class=\"page_wrapper\"]", "Body is loaded"
      fn_reset.apply this  
    # @evaluate ->
    #   __utils__.echo('Test')
    # __utils__.echo('Test')
    switch minicase
      when 0
        test.comment "Declare DayOff" #mark holiday and add reason
        test.assertExists '#link_day_off'
        @click '#link_day_off'
        fn_snap()
        @fill 'div[id="dialog_dayoff"]', 
            "name":"Teacher's Day"
            , true
          fn_snap()
        @click '#btn_affirm_declare_day_off'
        # @evaluate ->
        #   __utils__.exists 'div[id="dialog_dayoff"]'

        # console.log 'wait for dialog'
        # @waitUntilVisible 'div[id="dialog_dayoff"]', ->
        #   console.log 'fill form'
        #   @fill 'div[id="dialog_dayoff"]', 
        #     "name":"Teacher's Day"
        #     , true
        #   fn_snap()
      when 1
        test.comment "Cancel Attendance"
        #Cancel holiday then check start marking attendance.
        #a.take_att_link 
      # when 2
      when 3
        test.comment "Add Remark"   
        fn_snap()
        test.assertExists '#remark_a02e701a-cd04-11e2-9f78-bf2b9980b50b'
        @click xid('remark_a02e701a-cd04-11e2-9f78-bf2b9980b50b')
        @click 'table.list_item_selection a[rel="Late"]'
        @click 'table.list_item_selection a[rel="HFMD"]'
        @click 'table.list_item_selection a[rel="Red Spots Palms"]'
        #casper.capture 'scr.jpg'
        fn_snap()
        test.assertExists "#btn_done_remark"
        @click xid("btn_done_remark")
        fn_save.apply this
      when 4 
        test.comment "Before Taking Attendance"
        before = 
          is_absent: @getElementsAttribute('a.is_absent').length
          is_present: @getElementsAttribute('a.is_present').length
        u.dump before  
        fn_snap()
        test.assertExists xid 'a02e85d2-cd04-11e2-9f78-bf2b9980b50b'
        @click xid 'a02e85d2-cd04-11e2-9f78-bf2b9980b50b' #mark attendance
        test.assertExists xid 'a02e6016-cd04-11e2-9f78-bf2b9980b50b' 
        @click xid 'a02e6016-cd04-11e2-9f78-bf2b9980b50b' #mark attendance
        after = 
          is_absent: @getElementsAttribute('a.is_absent').length
          is_present: @getElementsAttribute('a.is_present').length
        test.comment "After taking attendance"  
        u.dump after
        fn_save.apply this
      when 5
        test.comment "take temperature"
        test.assertExists "td.temp_col li a.temp_ok"
        @click "td.temp_col li a.temp_ok"
        #problem with testing the temperature button
        fn_snap()
        @waitForSelector '#btn_done_temperature', ->
          test.assertExists xid "btn_done_temperature"
          @click xid 38
          @click xid 4 
          @click xid "btn_done_temperature"
        fn_snap()
  
  casper.run ->
    test.done()