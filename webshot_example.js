var webshot = require('webshot');

var options = {
	screenSize: {
		width: 1280
		, height: 680
	}, 
	shotSize: {
		width: 1280
		, height: 'all'
	}, 
	userAgent: 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_2 like Mac OS X; en-us)'
	+ ' AppleWebKit/531.21.20 (KHTML, like Gecko) Mobile/7B298g',
	pageSize: { 
		format: 'A4', 
		orientation: 'portrait', 
		border: '1cm' 
	}
}

webshot('vnexpress.net', 'vnexpress.png', options, function(err) {
  // screenshot now saved to flickr.jpeg
  console.log(err);
});
