/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Task configuration.
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        unused: true,
        boss: true,
        eqnull: true,
        globals: {}
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      lib_test: {
        src: ['lib/**/*.js', 'test/**/*.js']
      }
    },
    // nodeunit: {
    //   files: ['test/**/*_test.js']
    // },
    watch: {
      // gruntfile: {
      //   files: '<%= jshint.gruntfile.src %>',
      //   tasks: ['jshint:gruntfile']
      // },
      // lib_test: {
      //   files: '<%= jshint.lib_test.src %>',
      //   tasks: ['jshint:lib_test', 'nodeunit']
      // }
      coffeelint: {
          files: ['customize/*.*','coffee/*.*'],
          tasks: ['coffeelint:dev']
      },
      casper: {
          files: ['customize/attendance.*'],
          tasks: ['casper:attendance']
      }
    },
    casper : {
      options : {
        test : true,
        includes : 'customize/default.coffee',
        config : 'config/config.json',
        // post : 'path/to/post.js',
        // pre : 'path/to/pre.js',
        'log-level' : 'warning',
        'fail-fast' : false,
        'role':'superadmin',
        'minicase':1
      },
      attendance : {
        src: ['customize/attendance.coffee'],
        dest : function(input) {
          return input.replace(/\.js$/,'.xml');
        }
      }
    },
    coffeelint: {
      app: ['coffee/*.coffee', 'customize/*.coffee'],
      options: {
        'no_trailing_whitespace': {
          'level': 'ignore'
        },
        "max_line_length": {
            "value": 180,
            "level": "error"
        },
        "indentation": {
          "value": 2,
          "level": "warning"
        }
      },
      dev: {
        // tests: {
          files: {
            src: ['customize/*.coffee']
          },

        // }
      }
    }

  });

 

  // grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-coffeelint');
  grunt.loadNpmTasks('grunt-casper');
  // grunt-casper

  // Default task.
  grunt.registerTask('default', ['watch']);
  grunt.registerTask('coffee_lint', ['watch:coffeelint']);
  grunt.registerTask('casper_attendance', ['casper:attendance']);

};
