//==============================================================================
// Casper generated Fri Oct 04 2013 14:18:59 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
var casper = require('casper').create();
casper.options.viewportSize = {width: 1920, height: 995};
casper.start('http://staging.littlelives.com/schools/profile/25996/odyssey-fourth-avenue');
casper.waitForSelector(x("//*[contains(text(), 'As you entered Odyssey')]"),
    function success() {
        this.test.assertExists(x("//*[contains(text(), 'As you entered Odyssey')]"));
      },
    function fail() {
        this.test.assertExists(x("//*[contains(text(), 'As you entered Odyssey')]"));
});

casper.run(function() {this.test.renderResults(true);});