//==============================================================================
// Casper generated Fri Oct 04 2013 15:10:42 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
var casper = require('casper').create();
casper.options.viewportSize = {width: 1920, height: 995};
casper.start('https://staging.littlelives.com/attendances/roster/25800/20131004?h=2cb2e65ed7469a83e0874847a9d3176e');
casper.waitForSelector(x("//a[normalize-space(text())='Saving...']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Saving...']"));
        this.click(x("//a[normalize-space(text())='Saving...']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Saving...']"));
});
casper.waitForSelector(x("//*[contains(text(), 'Present 9')]"),
    function success() {
        this.test.assertExists(x("//*[contains(text(), 'Present 9')]"));
      },
    function fail() {
        this.test.assertExists(x("//*[contains(text(), 'Present 9')]"));
});
casper.waitForSelector(x("//*[contains(text(), 'Absent 1')]"),
    function success() {
        this.test.assertExists(x("//*[contains(text(), 'Absent 1')]"));
      },
    function fail() {
        this.test.assertExists(x("//*[contains(text(), 'Absent 1')]"));
});

casper.run(function() {this.test.renderResults(true);});