//==============================================================================
// Casper generated Fri Oct 04 2013 15:02:59 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
var casper = require('casper').create();
casper.options.viewportSize = {width: 1920, height: 995};
casper.start('http://staging.littlelives.com/schools/profile/25996/odyssey-fourth-avenue');
casper.waitForSelector(x("//a[normalize-space(text())='Manage your school photos']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Manage your school photos']"));
        this.click(x("//a[normalize-space(text())='Manage your school photos']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Manage your school photos']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='×']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='×']"));
        this.click(x("//a[normalize-space(text())='×']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='×']"));
});

casper.run(function() {this.test.renderResults(true);});