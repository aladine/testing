//==============================================================================
// Casper generated Fri Oct 04 2013 14:23:59 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
var casper = require('casper').create();
casper.options.viewportSize = {width: 1920, height: 995};
casper.start('https://staging.littlelives.com/questions?h=94ca92546784c6ffff5b3fdf5a021dae');
casper.waitForSelector("#ContentContent",
    function success() {
        this.test.assertExists("#ContentContent");
        this.click("#ContentContent");
    },
    function fail() {
        this.test.assertExists("#ContentContent");
});
casper.waitForSelector("textarea[name='data[Content][content]']",
    function success() {
        this.sendKeys("textarea[name='data[Content][content]']", "Hello Friends");
    },
    function fail() {
        this.test.assertExists("textarea[name='data[Content][content]']");
});
casper.waitForSelector("form#frmAnswer input#p186125c3boec16af12ar1spr10cp0_html5",
    function success() {
        this.test.assertExists("form#frmAnswer input#p186125c3boec16af12ar1spr10cp0_html5");
        this.click("form#frmAnswer input#p186125c3boec16af12ar1spr10cp0_html5");
    },
    function fail() {
        this.test.assertExists("form#frmAnswer input#p186125c3boec16af12ar1spr10cp0_html5");
});
casper.waitForSelector("#addPhoto",
    function success() {
        this.test.assertExists("#addPhoto");
        this.click("#addPhoto");
    },
    function fail() {
        this.test.assertExists("#addPhoto");
});
casper.waitForSelector(x("//*[contains(text(), 'Your answer has been posted!')]"),
    function success() {
        this.test.assertExists(x("//*[contains(text(), 'Your answer has been posted!')]"));
      },
    function fail() {
        this.test.assertExists(x("//*[contains(text(), 'Your answer has been posted!')]"));
});

casper.run(function() {this.test.renderResults(true);});