//==============================================================================
// Casper generated Fri Oct 04 2013 15:05:18 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
var casper = require('casper').create();
casper.options.viewportSize = {width: 1920, height: 995};
casper.start('http://staging.littlelives.com/schools/profile/25996/odyssey-fourth-avenue');
casper.waitForSelector(x("//a[normalize-space(text())='Manage your school photos']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Manage your school photos']"));
        this.click(x("//a[normalize-space(text())='Manage your school photos']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Manage your school photos']"));
});
casper.waitForSelector(x("//a[normalize-space(text())='Describe this photo']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Describe this photo']"));
        this.click(x("//a[normalize-space(text())='Describe this photo']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Describe this photo']"));
});
casper.waitForSelector(".input-medium",
    function success() {
        this.sendKeys(".input-medium", "test caption");
    },
    function fail() {
        this.test.assertExists(".input-medium");
});
casper.waitForSelector(".editable-submit.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-state-hover .ui-button-text",
    function success() {
        this.test.assertExists(".editable-submit.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-state-hover .ui-button-text");
        this.click(".editable-submit.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-state-hover .ui-button-text");
    },
    function fail() {
        this.test.assertExists(".editable-submit.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-state-hover .ui-button-text");
});
casper.waitForSelector(x("//a[normalize-space(text())='Close']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Close']"));
        this.click(x("//a[normalize-space(text())='Close']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Close']"));
});
casper.waitForSelector(".static-desc p:nth-child(4)",
    function success() {
        this.test.assertExists(".static-desc p:nth-child(4)");
        this.click(".static-desc p:nth-child(4)");
    },
    function fail() {
        this.test.assertExists(".static-desc p:nth-child(4)");
});
casper.waitForSelector(x("//*[contains(text(), 'test caption')]"),
    function success() {
        this.test.assertExists(x("//*[contains(text(), 'test caption')]"));
      },
    function fail() {
        this.test.assertExists(x("//*[contains(text(), 'test caption')]"));
});

casper.run(function() {this.test.renderResults(true);});