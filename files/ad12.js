//==============================================================================
// Casper generated Fri Oct 04 2013 14:14:05 GMT+0800 (Malay Peninsula Standard Time)
//==============================================================================

var x = require('casper').selectXPath;
var casper = require('casper').create();
casper.options.viewportSize = {width: 1600, height: 799};
casper.start('https://staging.littlelives.com/todos?h=05ac060da4a4fc9b95fe0d8ec36ea884');
casper.waitForSelector(x("//a[normalize-space(text())='Home']"),
    function success() {
        this.test.assertExists(x("//a[normalize-space(text())='Home']"));
        this.click(x("//a[normalize-space(text())='Home']"));
    },
    function fail() {
        this.test.assertExists(x("//a[normalize-space(text())='Home']"));
});
casper.waitForSelector("form input[name='end_dt']",
    function success() {
        this.test.assertExists("form input[name='end_dt']");
        this.click("form input[name='end_dt']");
    },
    function fail() {
        this.test.assertExists("form input[name='end_dt']");
});
casper.waitForSelector("#btn_dialog_withdraw .ui-button-text",
    function success() {
        this.test.assertExists("#btn_dialog_withdraw .ui-button-text");
        this.click("#btn_dialog_withdraw .ui-button-text");
    },
    function fail() {
        this.test.assertExists("#btn_dialog_withdraw .ui-button-text");
});

casper.run(function() {this.test.renderResults(true);});