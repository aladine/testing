# casperjs test convert.coffee
x = require('casper').selectXPath
u = require('utils')
f = require('fs')

# cases = fs.read('testcase3.json')
stream = fs.open './testcase3.json', 'r'
cases = stream.read()
# u.dump cases
obj
try
  obj = JSON.parse cases
  # u.dump obj
catch e
  console.error e


#remove unnecessary fields
q = Array::map.call obj, (e) ->
  delete e.title
  delete e.tester
  delete e.steps
  delete e.functionality
  delete e['']
  delete e['test role']
  delete e['test inputs']
  delete e['test procedure']
  delete e['actual results']
  e


#function write to a file for product group and with role
fn = (input,fn_check,file_name)->
  s = Array::filter.call input, (e) ->
    true if fn_check e, file_name
  w = JSON.stringify s, null ,'  '
  file_name = file_name.toLowerCase()
  fs.write "files/#{file_name}.json", w, 'w' 
fn_check = (e, name)->
  e.nid isnt null and e.url isnt "" and e.product is name


#WRITE TO: checkTitle json

#remove empty test cases
# s = Array::filter.call q, (e) ->
#   true if e.nid isnt null and e.url isnt ""


#WRITE TO: attendance json
fn q, fn_check, "Attendance"
fn q, fn_check, "Portfolio"
fn q, fn_check, "Admin"
fn q, fn_check, "Question"
fn q, fn_check, "Notification"
fn q, fn_check, "Login"

s = Array::filter.call q, (e) ->
  true if e.nid isnt null and e.url isnt "" and e.product isnt "" 
w = JSON.stringify s, null ,'  '
fs.write "files/all.json", w, 'w' 
