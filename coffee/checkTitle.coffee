# casperjs --config=./config/config.json test --includes=./customize/default.coffee --host=https://staging.littlelives.com coffee/checkTitle.coffee --set=all
x = require('casper').selectXPath
u = require('utils')
f = require('fs')
# m = require('mongoose') 
# uristring = 'mongodb://littledan:qwerty@ds047468.mongolab.com:47468/testcases'
# m.connect uristring, (err, res) ->
#   if err
#     console.log "ERROR connecting to: " + uristring + ". " + err
#   else
#     console.log "Succeeded connected to: " + uristring


# # This is the schema.  Note the types, validation and trim
# # statements.  They enforce useful constraints on the data.
# testcaseSchema = new mongoose.Schema(
#   nid:
#     type: Number
#     min: 0

#   product:
#     type: String
#     trim: true

#   expected_results:
#     type: String
#     trim: true

#   account:
#     type: String
#     trim: true

#   url:
#     type: String
#     trim: true

#   exist:
#     type: String
#     trim: true

#   fields:
#     type: String
#     trim: true

#   tid:
#     type: String
#     trim: true
# )

# Testcase = mongoose.model("Testcases", testcaseSchema)
# rel = Testcase.find({})
# console.log rel
# exit 0 
# __utils__.echo 'df'
casper.options.pageSettings["loadImages"]= true
casper.options.pageSettings["loadPlugins"]= true

# e.url = e.url.replace("staging.littlelives.com", "192.168.1.55").replace("https", "http")
stream = fs.open 'files/'+casper.cli.get("set")+'.json', 'r'
cases = stream.read()
try
  cases = JSON.parse cases
  # u.dump cases
catch e
  console.error e

host   = casper.cli.get 'host'
# host = 'http://192.168.1.55'
login_url = host+'/users/login'
tm = new Date()
tm = tm.getFullYear() + "_" + (tm.getMonth() + 1) + "_" + tm.getDate() + "_" + tm.getHours() + "_" + tm.getMinutes()
counter = 1
fn_snap = (tid)->
  n = new Date()
  # dir = 'casper'#'/Users/dan/Dropbox/Public/casper'
  # l = 'casper'#'https://dl.dropboxusercontent.com/u/22391606/casper'
  if typeof(tid) is "string" or typeof(tid) is "number"
    sc = "/screenshots/#{tm}_#{tid}.png"
  else
    sc = "/screenshots/#{tm}_#{counter++}.png"
  casper.viewport 1024, 768
  casper.capture casper.options.casperDir+sc,
    top: 0
    left: 0
    width: 1024 
    height: 768
  console.log "Snapshot: "
  console.log "#{casper.options.casperLink+sc}"
  console.log "Done at "+ casper.getCurrentTimestamp()

casper.test.begin "Checking Title", 2*cases.length, suite = (test) ->  
  casper.start login_url, ->
    # fn_snap()
    data = 
      'data[User][username]': casper.accounts.superadmin.username
      'data[User][password]': casper.accounts.superadmin.password
    test.assertExists 'form[action="/users/login"]', 'login form is found'
    @fill 'form[action="/users/login"]', data, true
    @click 'input[type="submit"]'
  
  casper.eachThen cases, (response) ->
    url = response.data.url
    t = response.data.title
    element = response.data.exist
    id = response.data.sn 
    tid = response.data.tid 

    console.log "-------------------"
    console.log "Test case #{tid}"
    console.log "Product: "+response.data.product
    console.log "Functionality: "+ response.data.functionality
    console.log "Opening " + url

    @thenOpen url, (res) ->
      # @waitForSelector "div[class=\"page_main\"]", (->
      href = @evaluate(->
        document.location.href
      )
      title = @evaluate(->
        document.title
      )
      console.log 'Href:  '+href
      console.log 'Title: '+title
      u.dump element
      # @test.assertExists "div[class=\"page_wrapper\"]", "Body is loaded"
      # if t?
      #   @test.assertTitle t

      # if u.isArray element 
      #   Array::map.call element, (e) ->
      #     console.log e
      #     casper.test.assertExists x(e), "Element exists ->"+x(e)
      # else @test.assertExists x(element), "Element exists ->"+x(element) if element isnt "" 

      @test.assertExists x(element), "Element exists ->"+x(element) if element isnt "" 
    
    # @snap tid
  casper.run ->
    test.done()
    u.dump @logs

